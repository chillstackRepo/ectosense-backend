const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateSignUp = (opts) => {
  try{
if((!opts.hasOwnProperty('role') ||
!opts.hasOwnProperty('phone') ||
!(opts.hasOwnProperty('doctor_password') || 
opts.hasOwnProperty('patient_password') || 
opts.hasOwnProperty('admin_password') || 
opts.hasOwnProperty('assistant_password')) )){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateSignUp
}