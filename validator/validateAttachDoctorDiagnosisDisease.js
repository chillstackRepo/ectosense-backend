const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateAttachDoctorDiagnosisDisease = (opts) => {
  try{
if((!opts.hasOwnProperty('doctor_id') || !opts.hasOwnProperty('disease_id'))){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateAttachDoctorDiagnosisDisease
}