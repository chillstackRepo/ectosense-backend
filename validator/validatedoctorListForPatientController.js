const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateDoctorListForPatient = (opts) => {
  try{
if((!opts.hasOwnProperty('patient_request_id') || !opts.hasOwnProperty('assistant_id'))){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateDoctorListForPatient
}