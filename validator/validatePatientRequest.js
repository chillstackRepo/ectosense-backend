const { PreconditionFailed } = require("..//errorClasses/errorClass");
const validatePatientRequest = (opts) => {
try{
if(
(!opts.hasOwnProperty('patient_id')) ||
(!opts.hasOwnProperty('disease_id')) ||
(!opts.hasOwnProperty('clinic_id'))  ||
(!opts.hasOwnProperty('time_slot'))  || 
(!opts.hasOwnProperty('patient_reports')) ){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validatePatientRequest
}