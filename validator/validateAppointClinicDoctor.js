const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateAppointClinicDoctor = (opts) => {
  try{
if((!opts.hasOwnProperty('doctor_id') ||
!opts.hasOwnProperty('clinic_id') ||
!opts.hasOwnProperty('relation_status'))){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateAppointClinicDoctor
}