const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateAssignAssistantToDoctor = (opts) => {
  try{
if((!opts.hasOwnProperty('doctor_id') ||
!opts.hasOwnProperty('assistant_id') ||
!opts.hasOwnProperty('relation_status'))){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateAssignAssistantToDoctor
}