const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateGetDoctorRequests = (opts) => {
  try{
if((!opts.hasOwnProperty('doctor_id'))){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateGetDoctorRequests
}