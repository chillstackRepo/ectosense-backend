const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateDoctorUpload = (opts,req) => {
  try{
    if((!opts.patient_request_id) ||
    !(req&&req.file&&req.file.location?req.file.location:null)){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateDoctorUpload
}