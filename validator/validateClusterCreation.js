const { PreconditionFailed } = require("..//errorClasses/errorClass");
const validateClusterCreation = (opts) => {
  try{
if((!opts.hasOwnProperty('lat') ||
!opts.hasOwnProperty('long') ||
!opts.hasOwnProperty('resolution') ||
!opts.hasOwnProperty('cluster_name'))){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateClusterCreation
}