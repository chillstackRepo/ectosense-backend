const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateLogin = (opts) => {
  try{
if((!opts.hasOwnProperty('role') ||
!opts.hasOwnProperty('phone') ||
!opts.hasOwnProperty('password'))){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateLogin
}