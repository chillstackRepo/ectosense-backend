const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateAssignDoctorToPatient = (opts) => {
  try{
if((!opts.hasOwnProperty('doctor_id') ||
!opts.hasOwnProperty('assistant_request_id') ||
!opts.hasOwnProperty('selected_time_slot'))){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateAssignDoctorToPatient
}