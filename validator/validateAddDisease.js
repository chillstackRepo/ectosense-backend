const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateAddDisease = (opts) => {
  try{
if((!opts.hasOwnProperty('disease_name') ||
!opts.hasOwnProperty('critical_level') ||
!opts.hasOwnProperty('test_required'))){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateAddDisease
}