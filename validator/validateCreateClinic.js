const { PreconditionFailed } = require("..//errorClasses/errorClass");

const validateCreateClinic = (opts) => {
  try{
if((!opts.hasOwnProperty('clinic_lat') ||
!opts.hasOwnProperty('clinic_long') ||
!opts.hasOwnProperty('resolution') ||
!opts.hasOwnProperty('clinic_name') ||
!opts.hasOwnProperty('county_code') ||
!opts.hasOwnProperty('clinic_phone') ||
!opts.hasOwnProperty('clinic_email') ||
!opts.hasOwnProperty('clinic_registration_number') ||
!opts.hasOwnProperty('clinic_start_time') ||
!opts.hasOwnProperty('clinic_end_time') ||
!opts.hasOwnProperty('clinic_pincode'))){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validateCreateClinic
}