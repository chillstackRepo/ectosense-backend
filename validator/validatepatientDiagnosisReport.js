const { PreconditionFailed } = require("..//errorClasses/errorClass");
const validatepatientDiagnosisReport = (opts) => {
  try{
if((!opts.hasOwnProperty('patient_request_id') ||
(!opts.hasOwnProperty('clinic_id')) ||
(!opts.hasOwnProperty('doctor_id'))
)){

  throw new PreconditionFailed("Please check the payload.");
}
else{
  return
}
}
  catch(e){
      throw(e)
  }
  
};


module.exports={
    validatepatientDiagnosisReport
}