var mysql = require('mysql'); // node module
var connectionDetails = require('../config');

var readConnectionPool = mysql.createPool({
  host: connectionDetails.prodDbReadConnection.host,
  user: connectionDetails.prodDbReadConnection.user,
  password: connectionDetails.prodDbReadConnection.password,
  database: connectionDetails.prodDbReadConnection.database,
  connectionLimit: connectionDetails.prodDbReadConnection.connectionLimit,
  port: connectionDetails.prodDbReadConnection.port,
  multipleStatements:true
});

var writeConnectionPool = mysql.createPool({
  host: connectionDetails.prodDbWriteConnection.host,
  user: connectionDetails.prodDbWriteConnection.user,
  password: connectionDetails.prodDbWriteConnection.password,
  database: connectionDetails.prodDbWriteConnection.database,
  connectionLimit: connectionDetails.prodDbWriteConnection.connectionLimit,
  port: connectionDetails.prodDbWriteConnection.port,
  multipleStatements:true
});

exports.writeResults = (query, valuesToEscape, cb) => {
  writeConnectionPool.getConnection(function (err, conn) {
    if (!err) {
      conn.query(query, valuesToEscape, function (error, results, fields) {
        if (error) {
          conn.release();
          return cb(error);
        } else {
          conn.release();
          return cb(null, { results: results, fields: fields });
        }
      });
    } else {
      console.log(err);
      throw (err);
    }
  });
};

exports.readResults = (query, valuesToEscape, cb) => {
  readConnectionPool.getConnection(function (err, conn) {
    if (!err) {
      conn.query(query, valuesToEscape, function (error, results, fields) {
        if (err) {
          conn.release();
          return cb(error);
        } else {
          conn.release();
          return cb(null, { results: results, fields: fields });
        }
      });
    } else {
      console.log(err);
      throw (err);
    }
  });
};
