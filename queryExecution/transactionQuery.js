var mysql = require('mysql');
const QUERY_TIMEOUT = 60000;
var connectionDetails = require('../config');


var readPool = mysql.createPool({
  host: connectionDetails.prodDbReadConnection.host,
  user: connectionDetails.prodDbReadConnection.user,
  password: connectionDetails.prodDbReadConnection.password,
  database: connectionDetails.prodDbReadConnection.database,
  connectionLimit: connectionDetails.prodDbReadConnection.connectionLimit,
  port: connectionDetails.prodDbReadConnection.port
});

var writePool = mysql.createPool({
  host: connectionDetails.prodDbWriteConnection.host,
  user: connectionDetails.prodDbWriteConnection.user,
  password: connectionDetails.prodDbWriteConnection.password,
  database: connectionDetails.prodDbWriteConnection.database,
  connectionLimit: connectionDetails.prodDbWriteConnection.connectionLimit,
  port: connectionDetails.prodDbWriteConnection.port,
  multipleStatements:true
});

//connectionWriteConfig.acquireTimeout = 10000; //default
//connectionWriteConfig.multipleStatements = true;


writePool.on('acquire', function(connection) {
  console.debug('Connection Acquired...'); // -1.);
});

writePool.on('release', function(connection) {
  console.debug('Connection Released... ', (writePool._freeConnections.indexOf(connection) != -1)); // -1: Not released
});

class Transaction {
  constructor(connection) {
    this.connection = connection;
  }
  query(queryString, escapedValues = []) {
    return new Promise((resolve, reject) => {
      const that = this;
      that.connection.query({sql: queryString, timeout: QUERY_TIMEOUT}, escapedValues, function (queryError, results, fields) {
        if (queryError) {
          console.debug('queryError', queryError);
          that.connection.rollback(function() {
            console.debug('Rollback...');
            that.connection.release();
            reject(queryError);
          });
        
        } else {
          resolve(results);
        }
      });
    });
  }
  commit() {
    return new Promise((resolve, reject) => {
      const that = this;
      that.connection.commit(function(commitError) {
        if (commitError) {
          console.debug('commitError', commitError);
          that.connection.rollback(function() {
            console.debug('Rollback...');
            that.connection.release();
            reject(commitError);
          });
        } else {
          that.connection.release();
          resolve();
        }
      });
    });
  }

  rollback() {
    return new Promise((resolve, reject) => {
      console.log("INSIDE ROLLBACK FUNCTION");
      const that = this;
      console.log("writePool._freeConnections.indexOf(that.connection) ",writePool._freeConnections.indexOf(that.connection)  ,writePool._freeConnections.indexOf(that.connection) );
      if(writePool._freeConnections.indexOf(that.connection) == -1) {
        console.log("IN ROLLBACK FUNCTION")
        that.connection.rollback(function() {
          console.debug('Rollback...');
          that.connection.release();
        });
      }
      resolve();
    });
  }

}

module.exports = {
  newTransaction: function() {
    return new Promise((resolve, reject) => {
      writePool.getConnection(function(connectError, connection) {
        if (connectError) reject(connectError);
        else {
          connection.beginTransaction(function(beginError) {
            if (beginError) reject(beginError);
            else {
              let transaction = new Transaction(connection);
              resolve(transaction);
            }
          });
        }
      });
    });
  },
  
  multiQuery: function (queryStrings) {
    return new Promise((resolve, reject) => {
      writePool.getConnection(function(connectError, connection) {
        if (connectError) reject(connectError);

        connection.beginTransaction(function(beginError) {
          if (beginError) reject(beginError);

          var combinedQuery = "";
          queryStrings.forEach((query) => combinedQuery = combinedQuery + query + ";");
          console.debug(combinedQuery);
          connection.query({sql: combinedQuery, timeout: QUERY_TIMEOUT}, function (queryError, results, fields) {
            if (queryError) {
              console.debug('queryError', queryError);
              connection.rollback(function() {
                console.info('Rollback...');
                connection.release();
                reject(queryError);
              });
            } else {
              connection.commit(function(commitError) {
              if (commitError) {
                console.debug('commitError', commitError);
                connection.rollback(function() {
                  console.info('Rollback...');
                  connection.release();
                  reject(commitError);
                });
              } else {
                console.debug('results', results);
                connection.release();
                resolve(results);
              }
            });
            }
          });
        });
      });
    });
  }
}
