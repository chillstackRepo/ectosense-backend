let queryExec = require('../connections/connection')
// /**
//  * @function readQuery to read prod db
//  * @param {query} queryString 
//  * @param {params} valuesToEscape 
//  * @returns {promice} resolve with data and reject with error
//  */

module.exports = {

    readQuery: function (queryString, valuesToEscape) {
        return new Promise((resolve, reject) => {
            queryExec.readResults(queryString, valuesToEscape,(err, data) => {
                if (err) reject(err);
                else resolve(data.results);          
            });
        })
    },

    writeQuery: function (queryString, valuesToEscape) {
        return new Promise((resolve, reject) => {
            queryExec.writeResults(queryString,valuesToEscape,(err, data) => {
                if (err) reject(err);
                else resolve(data);
            });
        })
    }
}
