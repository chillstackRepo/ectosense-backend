define({ "api": [
  {
    "type": "post",
    "url": "/api/v1/get-assistant-requests",
    "title": "",
    "name": "Asistant/Assistant_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Assistant-Service",
    "description": "<h2>Get all assistant requests.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assistant_id",
            "description": "<p>Mandatory : assistant_id of the assistant.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"successfully fetched assistant requests.\",\n  \"status\": 200,\n \"data\": [\n    {\n       \"id\": 1,\n      \"assistant_id\": 1,\n      \"patient_request_id\": 13,\n      \"request_status\": \"Pending\",\n      \"created_dt\": 1602886118,\n      \"created_by\": 1,\n      \"updated_dt\": 1602886118,\n      \"updated_by\": 1,\n     \"updated_by_designation\": null\n   },\n  {\n     \"id\": 2,\n     \"assistant_id\": 1,\n     \"patient_request_id\": 11,\n     \"request_status\": \"Pending\",\n     \"created_dt\": 1602886286,\n     \"created_by\": 1,\n     \"updated_dt\": 1602886286,\n     \"updated_by\": 1,\n     \"updated_by_designation\": null\n   }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. successfully fetched assistant requests..</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/assistantController/assistantRequest.js",
    "groupTitle": "Assistant-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/appoint-assistant-in-clinic",
    "title": "",
    "name": "Assistant/Assistant_Appointing",
    "permission": [
      {
        "name": "ADMIN"
      }
    ],
    "group": "Assistant-Service",
    "description": "<h2>Attach Assistant with clinic.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assistant_id",
            "description": "<p>Mandatory : Registered Assistant ID.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_id",
            "description": "<p>Mandatory : Registered Clinic ID.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relation_status",
            "description": "<p>Mandatory : active/inactive.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Assistant successfully assigned with clinic 5.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Assistant successfully assigned with clinic 5.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/clinicController/appointClinicAssistant.js",
    "groupTitle": "Assistant-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/assign-assistant-to-doctor",
    "title": "",
    "name": "Assistant/Assistant_Assignment",
    "permission": [
      {
        "name": "ADMIN"
      }
    ],
    "group": "Assistant-Service",
    "description": "<h2>Attach Assistant with clinic.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assistant_id",
            "description": "<p>Mandatory : Registered Assistant ID.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "doctor_id",
            "description": "<p>Mandatory : Registered Doctor ID.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relation_status",
            "description": "<p>Mandatory : active/inactive.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Assistant successfully assigned with doctor 5.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Assistant successfully assigned with doctor 5.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/clinicController/assignDoctorAssistant.js",
    "groupTitle": "Assistant-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/get-doctor-list-for-assistant",
    "title": "",
    "name": "Assistant/Assistant_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Assistant-Service",
    "description": "<h2>Get doctor list for assistant.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "patient_request_id",
            "description": "<p>Mandatory : patient_request_id of the patient.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assistant_id",
            "description": "<p>Mandatory : assistant_id of the patient assistant.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Successfully fetch doctor details for patient.\",\n  \"status\": 200,\n  \"data\": []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. successfully fetched doctor details.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/assistantController/assistantController.js",
    "groupTitle": "Assistant-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/get-assistant-details",
    "title": "",
    "name": "Assistant/Assistant_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Assistant-Service",
    "description": "<h2>Get assistant all details.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assistant_id",
            "description": "<p>Mandatory : assistant_id of the assistant.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"successfully fetched doctor requests.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. successfully fetched assistant details.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/assistantController/assistantDetails.js",
    "groupTitle": "Assistant-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/assign-doctor-to-patient",
    "title": "",
    "name": "Assistant/Assistant_Request",
    "permission": [
      {
        "name": "Assistant/Admin"
      }
    ],
    "group": "Assistant-Service",
    "description": "<h2>Appoint doctor for patient.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "doctor_id",
            "description": "<p>Mandatory : doctor_id of the patient assistant.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assistant_request_id",
            "description": "<p>Mandatory : assistant_request_id of assistant.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "selected_time_slot",
            "description": "<p>Mandatory : selected_time_slot by the assistant.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Appointment successfully scheduled.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. successfully fetched doctor details.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/assistantController/doctorAssignment.js",
    "groupTitle": "Assistant-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/get-doctor-schedules",
    "title": "",
    "name": "Assistant/Assistant_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Assistant-Service",
    "description": "<h2>Get doctor schedules.</h2> Repository: \"extosence\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "doctor_id",
            "description": "<p>Mandatory : doctor_id of the patient assistant.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Successfully fetched today's doctor Schedules.\",\n  \"status\": 200,\n  \"data\": []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. successfully fetched doctor details.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/doctorController/doctorSchedules.js",
    "groupTitle": "Assistant-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/create-clinic",
    "title": "",
    "name": "Clinic/Create_Clinic",
    "permission": [
      {
        "name": "ADMIN"
      }
    ],
    "group": "Clinic-Service",
    "description": "<h2>Create a new Clinic.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_lat",
            "description": "<p>Mandatory e.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_long",
            "description": "<p>Mandatory <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "resolution",
            "description": "<p>Mandatory <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_name",
            "description": "<p>Mandatory <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "county_code",
            "description": "<p>Mandatory <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_phone",
            "description": "<p>Mandatory <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_email",
            "description": "<p>Mandatory <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_registration_number",
            "description": "<p>Mandatory <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_start_time",
            "description": "<p>Mandatory <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_end_time",
            "description": "<p>Mandatory <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_pincode",
            "description": "<p>Mandatory <br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Successfully created Heart Care clinic in cluster 5.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Successfully created Heart Care clinic in cluster 5.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/clinicController/clinicController.js",
    "groupTitle": "Clinic-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/create-cluster",
    "title": "",
    "name": "Clinic/Create_Clinic",
    "permission": [
      {
        "name": "ADMIN"
      }
    ],
    "group": "Clinic-Service",
    "description": "<h2>Create a new Cluster.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lat",
            "description": "<p>Mandatory : Location Lattitude.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "long",
            "description": "<p>Mandatory : Location Longnitude.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cluster_name",
            "description": "<p>Mandatory : Cluster Name <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "resolution",
            "description": "<p>Mandatory : Uber H3 resolutions for hexagon area. <br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Successfully created Electronic City cluster with resolution level 7.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Successfully created Electronic City cluster with resolution level 7.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/h3Controller/createCluster.js",
    "groupTitle": "Clinic-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/add-disease",
    "title": "",
    "name": "Doctor/Add_disease",
    "permission": [
      {
        "name": "ADMIN"
      }
    ],
    "group": "Doctor-Service",
    "description": "<h2>Attach Disease in treatment list.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "disease_name",
            "description": "<p>Mandatory : Disease Name for identification.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "critical_level",
            "description": "<p>Mandatory : Disease Criticality.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "test_required",
            "description": "<p>Mandatory : 1/0.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Successfully Added Disease category named Kidney Disease.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Successfully Added Disease category named Kidney Disease.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/adminController/diseaseController.js",
    "groupTitle": "Doctor-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/appoint-doctor-in-clinic",
    "title": "",
    "name": "Doctor/Doctor_Appointing",
    "permission": [
      {
        "name": "ADMIN"
      }
    ],
    "group": "Doctor-Service",
    "description": "<h2>Attach doctor with clinic.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "doctor_id",
            "description": "<p>Mandatory : Registered Doctor ID.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_id",
            "description": "<p>Mandatory : Registered Clinic ID.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relation_status",
            "description": "<p>Mandatory : active/inactive.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Doctor successfully assigned with clinic 5.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Doctor successfully assigned with clinic 5.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/clinicController/appointClinicDoctor.js",
    "groupTitle": "Doctor-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/disease-diagnose-by-doctor",
    "title": "",
    "name": "Doctor/Doctor_Onboarding",
    "permission": [
      {
        "name": "ADMIN"
      }
    ],
    "group": "Doctor-Service",
    "description": "<h2>Attach disease treatmentwith Doctor.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "doctor_id",
            "description": "<p>Mandatory : Registered Doctor ID.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "disease_id",
            "description": "<p>Mandatory : Registered Disease Treatments ID.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Reports successfully Downloaded.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Dr. Meena is successfully registered for diagnose Heart Disease.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/doctorController/doctorDiagnosisDisease.js",
    "groupTitle": "Doctor-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/get-doctor-details",
    "title": "",
    "name": "Doctor/Doctor_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Doctor-Service",
    "description": "<h2>Get doctor all details.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "doctor_id",
            "description": "<p>Mandatory : doctor_id of the doctor.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"successfully fetched doctor requests.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. successfully fetched doctor details.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/doctorController/doctorDetails.js",
    "groupTitle": "Doctor-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/get-doctor-requests",
    "title": "",
    "name": "Doctor/Doctor_Request",
    "permission": [
      {
        "name": "Doctors"
      }
    ],
    "group": "Doctor-Service",
    "description": "<h2>Get doctor all requests.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "doctor_id",
            "description": "<p>Mandatory : doctor_id of the assistant.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"successfully fetched doctor requests.\",\n  \"status\": 200,\n  \"data\": []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. successfully fetched doctor requests..</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/doctorController/doctorRequests.js",
    "groupTitle": "Doctor-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/doctor-prescription-uploader",
    "title": "",
    "name": "Doctor/Doctor_Request",
    "permission": [
      {
        "name": "Doctor"
      }
    ],
    "group": "Doctor-Service",
    "description": "<h2>Upload prescription by doctor.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "patient_request_id",
            "description": "<p>Mandatory : patient_request_id of the patient.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "file",
            "description": "<p>Mandatory : prescription of the patient.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Prescription successfully uploaded.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Prescription successfully uploaded.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/doctorController/doctorUploadController.js",
    "groupTitle": "Doctor-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/post-doctor-diagnosis",
    "title": "",
    "name": "Doctor/Doctor_Request",
    "permission": [
      {
        "name": "Doctor"
      }
    ],
    "group": "Doctor-Service",
    "description": "<h2>Complete case by doctor.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "doctor_request_id",
            "description": "<p>Mandatory : doctor_request_id of the doctor.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Prescription successfully uploaded.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Prescription successfully uploaded.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/doctorController/postDiagnosis.js",
    "groupTitle": "Doctor-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/login",
    "title": "",
    "name": "Login",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Generic-Service",
    "description": "<h2>Login for patient/doctor/assistant/admin.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": "<p>Mandatory : User Role.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Mandatory : Registered Mobile number.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mandatory : Passcode <br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Successfully logged in.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Successfully logged in.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/authController/loginController.js",
    "groupTitle": "Generic-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/registration",
    "title": "",
    "name": "Registration",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Generic-Service",
    "description": "<h2>SignUp for patient/doctor/assistant/admin.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": "<p>Mandatory : User Role.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Mandatory : Registered Mobile number.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "doctor_password/patient_password/assistant_password/admin_password",
            "description": "<p>Mandatory : Passcode <br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Successfully signed up.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Successfully signed up.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"Already registered, Please try to login.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/authController/registrationController.js",
    "groupTitle": "Generic-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/get-patient-details",
    "title": "",
    "name": "Patient/Patient_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Patient-Service",
    "description": "<h2>Get patient all details.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "patient_id",
            "description": "<p>Mandatory : patient_id of the patient.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"successfully fetched doctor requests.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. successfully fetched patient details.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/patientController/patientDetails.js",
    "groupTitle": "Patient-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/patient-diagnosis-report-share",
    "title": "",
    "name": "Patient/Patient_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Patient-Service",
    "description": "<h2>Patient Report share.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "patient_request_id",
            "description": "<p>Mandatory : patient_request_id of the patient.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_id",
            "description": "<p>Mandatory : clinic_id of the doctor.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "doctor_id",
            "description": "<p>Mandatory : doctor_id of the doctor.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Shared Successfully.\",\n  \"status\": 200,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Shared Successfully..</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/patientController/patientDiagnosisReportController.js",
    "groupTitle": "Patient-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/patient-report-downloader",
    "title": "",
    "name": "Patient/Patient_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Patient-Service",
    "description": "<h2>Download patient reports.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fileUrl",
            "description": "<p>Mandatory : S3 link of the patient reports.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Reports successfully Downloaded.\",\n  \"status\": 200,\n  \"file\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Reporst successfully Downloaded.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/patientController/patientDownloadController.js",
    "groupTitle": "Patient-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/patient-treatment-request",
    "title": "",
    "name": "Patient/Patient_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Patient-Service",
    "description": "<h2>Create Patient Request.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "patient_id",
            "description": "<p>Mandatory : patient_id of the patient.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "disease_id",
            "description": "<p>Mandatory : disease_id of the patient.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clinic_id",
            "description": "<p>Mandatory : clinic_id of the patient.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "time_slot",
            "description": "<p>Mandatory : expected time_slot of the patient.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "patient_reports",
            "description": "<p>Mandatory : patient_reports of the patient.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Successfully created treatment requests.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Successfully created treatment requests.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/patientController/patientRequestController.js",
    "groupTitle": "Patient-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/get-patient-requests",
    "title": "",
    "name": "Patient/Patient_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Patient-Service",
    "description": "<h2>Get Patient all requests.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "patient_id",
            "description": "<p>Mandatory : doctor_id of the patient.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"successfully fetched patient requests.\",\n  \"status\": 200,\n  \"data\": []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. successfully fetched patient requests..</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/patientController/patientRequests.js",
    "groupTitle": "Patient-Service"
  },
  {
    "type": "post",
    "url": "/api/v1/patient-report-uploader",
    "title": "",
    "name": "Patient/Patient_Request",
    "permission": [
      {
        "name": "ALL"
      }
    ],
    "group": "Patient-Service",
    "description": "<h2>Upload reports by doctor.</h2> Repository: \"ectosense\"<br> Ticket Id : None <br> Ticket Link : None <br> <hr>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "patient_request_id",
            "description": "<p>Mandatory : patient_request_id of the patient.<br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "file",
            "description": "<p>Mandatory : reports of patient.<br></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "json",
            "optional": false,
            "field": "response-body",
            "description": "<p>body attached in success example.</p>"
          },
          {
            "group": "Success",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   \"message\": \"Reports successfully uploaded.\",\n  \"status\": 200,\n  \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Forbidden": [
          {
            "group": "Forbidden",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Ex. Reports successfully uploaded.</p>"
          },
          {
            "group": "Forbidden",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>403</p>"
          }
        ],
        "InternalServerError": [
          {
            "group": "InternalServerError",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Something went wrong.</p>"
          },
          {
            "group": "InternalServerError",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>500</p>"
          }
        ],
        "Unauthorized": [
          {
            "group": "Unauthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>You are not logged in! Please log in to get access.</p>"
          },
          {
            "group": "Unauthorized",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>401</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n \"You do not have permission to perform this action.\"\n}\nHTTP/1.1 500 InternalServerError\n{\n \"Something went wrong.\"\n}\nHTTP/1.1 401 Unauthorized\n{\n \"You are not logged in! Please log in to get access.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "controllers/patientController/patientUploadController.js",
    "groupTitle": "Patient-Service"
  }
] });
