/**
 * All required ClientSide Error classes 4XX series 
 */

class BadRequest extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, BadRequest)
      }
  
      this.name = 'BadRequest'
      this.status=400;
    }
  }
  class Unauthorized extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, Unauthorized)
      }
  
      this.name = 'Unauthorized'
      this.status=401;
    }
  }

  class PaymentRequired extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, PaymentRequired)
      }
  
      this.name = 'PaymentRequired'
      this.status=402;
    }
  }


  class Forbidden extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, Forbidden)
      }
  
      this.name = 'Forbidden'
      this.status=403;
    }
  }

  class NotFound extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, NotFound)
      }
  
      this.name = 'NotFound'
      this.status=404;
    }
  }

  class MethodNotAllowed extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, MethodNotAllowed)
      }
  
      this.name = 'MethodNotAllowed'
      this.status=405;
    }
  }

  class RequestTimeout extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, RequestTimeout)
      }
  
      this.name = 'RequestTimeout'
      this.status=408;
    }
  }

  class PayloadTooLarge extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, PayloadTooLarge)
      }
  
      this.name = 'PayloadTooLarge'
      this.status=413;
    }
  }

  class PreconditionFailed extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, PreconditionFailed)
      }
  
      this.name = 'PreconditionFailed'
      this.status=412;
    }
  }

  class URITooLong extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, URITooLong)
      }
      this.name = 'URITooLong'
      this.status=414;
    }
  }

  class RequestHeaderFieldsTooLarge extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, RequestHeaderFieldsTooLarge)
      }  
      this.name = 'RequestHeaderFieldsTooLarge'
      this.status=431;
    }
  }



/**
 * All required ServerSide Error classes 5XX series 
 */

  class InternalServerError extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, InternalServerError)
      } 
      this.name = 'InternalServerError'
      this.status=500;
    }
  }

  class NotImplemented extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, NotImplemented)
      }
      this.name = 'NotImplemented'
      this.status=501;
    }
  }

  class BadGateway extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, BadGateway)
      }
      this.name = 'BadGateway'
      this.status=502;
    }
  }
  class ServiceUnavailable extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, ServiceUnavailable)
      }
      this.name = 'ServiceUnavailable'
      this.status=503;
    }
  }

  class NoContent extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, NoContent)
      }
      this.name = 'NoContent'
      this.status=204;
    }
  }

/**
 * All required ServerSide success classes 2XX series 
 */
  class PartialContent extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, PartialContent)
      }
  
      this.name = 'PartialContent'
      this.status=206;
    }
  }


  class TemporaryRedirect extends Error {
    constructor(...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, TemporaryRedirect)
      }
  
      this.name = 'TemporaryRedirect'
      this.status=307;
    }
  }

module.exports={ 
    BadRequest,
    Unauthorized,
    PaymentRequired,
    Forbidden,
    NotFound,
    MethodNotAllowed,
    RequestTimeout,
    PayloadTooLarge,
    PreconditionFailed,
    URITooLong,
    RequestHeaderFieldsTooLarge,
    InternalServerError,
    NotImplemented,
    BadGateway,
    ServiceUnavailable,
    NoContent,
    PartialContent, 
    TemporaryRedirect 

}