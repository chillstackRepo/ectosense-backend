/**
 * Utility function for try catch block to be used in all controllers
 * @param  {function} fn Take a callback function
 * @return {function} fn Return function with next block attached
 */
module.exports = fn => {
    return (req, res, next) => {
      fn(req, res, next).catch(next);
    };
  };
  