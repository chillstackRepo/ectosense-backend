let TransactionQueryService = require("../queryExecution/transactionQuery");
let {readQuery}=require("../queryExecution/prodQueryExecution")
let doctorTimeSlot = async()=>{
    try{
        let query=[];
        let previous_records_query=[]
        let doctor_query = `select * from doctors where doctor_status='active';`
        let doctor_list = (await readQuery(doctor_query,[]));
       
       
        for(let i=0;i<doctor_list.length;i++){
            let doctor = doctor_list[i];
            let doctor_id = doctor.id;
            console.log(doctor)
            previous_records_query.push(`update doctor_schedules set time_slot_status = 1 where doctor_id = ${doctor_id} and time_slot_status = 0 and created_dt < unix_timestamp(now())`)

            let start_time = doctor.day_start_time;
            let end_time = doctor.day_end_time;
            
            for(let k = start_time;k<end_time;k+=1800){
                let time_slot_id = k/1800
                query.push(`insert into doctor_schedules(doctor_id,time_slot_id,time_slot_status,created_dt,created_by,updated_dt,updated_by)values(${doctor_id},${time_slot_id},0,unix_timestamp(now()),1,unix_timestamp(now()),1)`)
           }
   
        }
        await TransactionQueryService.multiQuery(previous_records_query)
        await TransactionQueryService.multiQuery(query);


    }catch(e){
        throw(e)
    }
}

doctorTimeSlot()