const config = require('../../config')
const mailjet = require ('node-mailjet').connect(config.emailServiceConfig.tocken_1,config.emailServiceConfig.tocken_2)

let sendEmail =  function(patient_request_id,clinic_email,clinic_name,doctor_email,doctor_name,cb){
 try{   
const request = mailjet.post("send").request(
    {
          "FromEmail": "rajat.singh@yulu.bike",
          "FromName": 'Ectosense',
          "Subject": 'Patient Report!',        
          "HTML-part": `
          <h4> 
          Hi Dr. ${doctor_name}, <br>
          Urgent care clinics or minor emergency setups are springing up all over riding on the need for quick medical assistance and owing to the waiting periods at regular hospitals. One of the main difference between these clinics and regular hospitals is these clinics do not take Medicaid, and hence are not required to attend to all patients. </h4>
       
          <h2> Requested Clinic : ${clinic_name}</h2>          
          <h2> Emergency Patient Request : ${patient_request_id}</h2>

          <table width="100%" cellspacing="0" cellpadding="0">
             <tr>
                 <td>
                     <table cellspacing="0" cellpadding="0">
                         <tr>
                             <td style="border-radius: 2px;" bgcolor="#ED2939">
                                 <a href="https://www.ectosense.com/" target="_blank" style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                                     Check Patient Request              
                                 </a>
                             </td>
                         </tr>
                     </table>
                 </td>
             </tr>`,
          "Recipients": [
              {
                  'Email': `${clinic_email}`
                },
             {
                 'Email': `${doctor_email}`
                }
            ]
    })
    request.then(result => {
        console.log(result.body)
        cb(null, result.body);
    })
    .catch(err => {
        console.log(err)
        cb("err");
    })  

}
catch(e){
    throw(e)

}

}


module.exports={
    sendEmail   
}

