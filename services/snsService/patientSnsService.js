let AWS = require('aws-sdk');
const config = require('../../config')

AWS.config.update({
    accessKeyId: config.awsConfig.awsAccessKeyId,
    secretAccessKey: config.awsConfig.awsSecretKey,
    region: config.awsConfig.awsRegion
});
let pushToPatientSNS = async(data)=>{
try{
// Create publish parameters
var params = {
    Message: `{"patient_request_id":${data.patient_request_id}}`, /* required */
    TopicArn: config.snsNotificationConfig.TopicArn
  };
  
  // Create promise and SNS service object
  var publishTextPromise = new AWS.SNS({apiVersion: '2010-03-31'}).publish(params).promise();
  
// Handle promise's fulfilled/rejected states
publishTextPromise.then(
    function(data) {
      console.log(`Message ${params.Message}  sent to the topic ${params.TopicArn}`);
      console.log("MessageID is " + data.MessageId);
    }).catch(
      function(err) {
      console.error(err, err.stack);
    });
}catch(e){
    throw(e)
}
}

module.exports={pushToPatientSNS}