const h3 = require("h3-js");
const {writeQuery, readQuery}=require("../../queryExecution/prodQueryExecution")
let clusterCreationService = async(opts,operator,role)=>{
try{
    let lat = opts.lat;
        let long = opts.long;
        let cluster_name = opts.cluster_name;
        let resolution = opts.resolution
        let h3_index = h3.geoToH3(lat, long, resolution);
        let query = `insert into clusters(cluster_name,resolution,h3_index,created_by,created_dt,updated_by,updated_dt,updated_by_designation) VALUES(?,?,?,1,unix_timestamp(now()),?,unix_timestamp(now()),?)`
        let {results} = await writeQuery(query,[cluster_name,resolution,h3_index,operator,role])
        let {insertId} = results;
        return {cluster_id:insertId,h3_index:h3_index,cluster_name:cluster_name }
}catch(e){
    throw(e)
}
}
let checkPreviousClusters = async(opts)=>{
    try{
            let lat = opts.lat;
            let long = opts.long;
            let resolution = opts.resolution
            let h3_index = h3.geoToH3(lat, long, resolution);
            let query = `select * from clusters where h3_index=?`
            let results = await readQuery(query,[h3_index])
            if(results.length){
                let cluster_details = results.pop()  
                return {isAlreadyPresent:true,cluster:cluster_details.id}
                }
            else
                return {isAlreadyPresent:false}
    }catch(e){
        throw(e)
    }
    }
    

module.exports={
    clusterCreationService,
    checkPreviousClusters
}