let {writeQuery}=require("../../queryExecution/prodQueryExecution")
let uploadDoctorPrescriptionService = async(patient_requests_id,reports,operator,role)=>{
    try{
        let query = `update patient_requests set prescription = ?,updated_dt=unix_timestamp(now()),updated_by=?,updated_by_designation = ? where id =? ;`
        await writeQuery(query,[reports,operator,role,patient_requests_id])
        return;

    }catch(e){
        throw(e)
    }
}

module.exports={
    uploadDoctorPrescriptionService
}
 