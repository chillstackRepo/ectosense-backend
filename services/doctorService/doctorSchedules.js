const { readQuery } = require("../../queryExecution/prodQueryExecution")

let getDoctorSchedulesService = async(doctor_id)=>{
try{
    let query = `select 
    doctor_schedules.doctor_id,
    from_unixtime(time_slots.diagnosis_time,'%H:%i:%s'),
    doctor_schedules.time_slot_status
    from doctor_schedules,time_slots where doctor_schedules.time_slot_id=time_slots.id 
    and doctor_id = ?
    and from_unixtime(unix_timestamp(now()),'%Y %D %M') = from_unixtime(doctor_schedules.created_dt,'%Y %D %M')`
    let schedules  = await readQuery(query,[doctor_id]);
    return schedules;
}catch(e){
    throw(e)
}
}

module.exports={
    getDoctorSchedulesService
}