let {readQuery}=require("../../queryExecution/prodQueryExecution")
let getDoctorDetailsService = async(doctor_id)=>{
    try{
        let query = `select id,doctor_name,county_code,doctor_phone,doctor_gender,doctor_specialization,doctor_fee,doctor_status,day_start_time,day_end_time,created_dt,doctor_city,doctor_lat,doctor_long,doctor_registration_number from doctors where id = ?;`
        let details = await readQuery(query,[doctor_id])
        return details;        
    }catch(e){
        throw(e)
    }
}

module.exports={getDoctorDetailsService}