let {readQuery} = require("../../queryExecution/prodQueryExecution")
let TransactionQueryService = require("../../queryExecution/transactionQuery");
const { Forbidden } = require("../../errorClasses/errorClass");
let postDiagnosisService = async(doctor_request_id,operator,role)=>{
    try{
        let request_details_query = `select * from doctor_requests where id=?`;
        let d_request_details = await readQuery(request_details_query,[doctor_request_id]);
        let request_details ;
        let already_processed =false;
        if(d_request_details.length){
            request_details = d_request_details.pop();
            already_processed = (request_details.request_status=='Completed')?true:false

        }else{
            throw new Forbidden("Invalid request to process")
        }
        if(request_details.doctor_id!=operator){
            throw new Forbidden("You can not process other doctor requests")

        }
        let patient_request_id = request_details.patient_request_id;
        let assistant_request_id = request_details.assistant_request_id;
        let prescription_query = `select prescription from patient_requests where id=?`;
        let prescription_details = (await readQuery(prescription_query,[patient_request_id])).pop();
        if(!prescription_details.prescription)
            throw(new Forbidden("PLease upload prescription to complete this diagnosis."))
        else{
        let query = [];
        query.push(`update doctor_requests set request_status='Completed',updated_dt=unix_timestamp(now()),updated_by=${operator},updated_by_designation='${role}' where id=${doctor_request_id}`)
        query.push(`update patient_requests set request_status='Completed',updated_dt=unix_timestamp(now()),updated_by=${operator},updated_by_designation='${role}' where id=${patient_request_id}`)
        query.push(`update assistant_requests set request_status='Completed',updated_dt=unix_timestamp(now()),updated_by=${operator},updated_by_designation='${role}' where id=${assistant_request_id}`)
       
        await TransactionQueryService.multiQuery(query);
        }
        return({
            patient_request_id:patient_request_id,
            assistant_request_id:assistant_request_id,
            doctor_request_id:doctor_request_id,
            doctor_id:request_details.doctor_id,
            already_processed:already_processed
        })

    }catch(e){
        throw(e)
    }
}


module.exports={
    postDiagnosisService
}