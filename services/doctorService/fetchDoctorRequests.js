let {readQuery} = require("../../queryExecution/prodQueryExecution")
let getDoctorRequestService = async(doctor_id)=>{
    try{
        let query = `select * from doctor_requests where doctor_id = ? and request_status='Pending';`
        let doctor_request_details = await readQuery(query,[doctor_id])
        return doctor_request_details;
    }catch(e){
        throw(e)
    }
}

module.exports={
    getDoctorRequestService
}