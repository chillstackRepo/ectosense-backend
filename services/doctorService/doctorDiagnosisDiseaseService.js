const {writeQuery, readQuery} = require("../../queryExecution/prodQueryExecution");
const { Forbidden } = require("../../errorClasses/errorClass");
let attachDoctorDiagnosisDiseaseService = async(opts,operator,role)=>{
let doctor_id = opts.doctor_id;
let disease_id = opts.disease_id;
let doctor_query = `select id,doctor_name from doctors where id =?;`
let disease_query = `select id,disease_name from diseases where id =?;`
let doctor_details = await readQuery(doctor_query,[doctor_id])
let disease_details = await readQuery(disease_query,[disease_id])
if(doctor_details.length && disease_details.length){
let {doctor_name} = doctor_details.pop();
let {disease_name} = disease_details.pop();
let query = `insert into doctor_disease_map(doctor_id,disease_id,created_by,created_dt,updated_by,updated_dt,updated_by_designation) VALUES(?,?,?,unix_timestamp(now()),?,unix_timestamp(now()),?)`
let {results} = await writeQuery(query,[doctor_id,disease_id,operator,operator,role])
let {insertId} = results;
if(insertId)
    return {status:true,doctor_name:doctor_name,disease_name:disease_name};
else    
    return {status:false};
}
else{
throw new Forbidden("Please check with cluster manager to get clear picture of Issue.")
}
}
module.exports={
    attachDoctorDiagnosisDiseaseService
}