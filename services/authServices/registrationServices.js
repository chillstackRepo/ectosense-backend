const { readQuery,writeQuery } = require('../../queryExecution/prodQueryExecution');
const {secretKey} = require('../../config').registrationControllerConfig;
let jwt = require('jsonwebtoken');
let checkIfDoctorAlreadyRegistered=async(phone)=>{
let doctor_info = `select id from doctors where doctor_phone=?;`
let doctor = await readQuery(doctor_info,phone);
if(doctor && doctor.length){
    return true;
}else{
    return false;

}
}

let checkIfAssistantAlreadyRegistered=async(phone)=>{
    let assistant_info = `select id from assistants where assistant_phone=?;`
    let assistant = await readQuery(assistant_info,phone);
    if(assistant.length){
        return true;
    } else{
        return false;
    }

}

let checkIfAdminAlreadyRegistered=async(phone)=>{
    let admin_info = `select id from admins where admin_phone=?;`
    let admin = await readQuery(admin_info,phone);
    if(admin.length){
        return true;
    } else{
        return false;
    }

}


    let checkIfPatientAlreadyRegistered=async(phone)=>{
        let patient_info = `select id from patients where patient_phone=?;`
        let patient = await readQuery(patient_info,phone);
        if(patient.length){
            return true;
        }else{
            return false;

        }
        }
        

let doctorRegistration=async(opts)=>{
try{
let doctor_phone = opts.phone;
let doctor_name = opts.doctor_name;
let doctor_gender = opts.doctor_gender;
let doctor_password = opts.doctor_password;
let doctor_city = opts.doctor_city;
let county_code = opts.county_code;
let doctor_lat = opts.doctor_lat;
let doctor_long = opts.doctor_long;
let doctor_pincode = opts.doctor_pincode;
let doctor_registration_number = opts.doctor_registration_number;
let doctor_specialization = opts.doctor_specialization;
let day_start_time = opts.day_start_time;
let day_end_time = opts.day_end_time;
let doctor_rating = 5;
let doctor_status = 'active';
let query = `insert into doctors (doctor_password,doctor_name,doctor_gender,doctor_phone,doctor_city,county_code,doctor_lat,doctor_long,doctor_pincode,doctor_registration_number,doctor_specialization,day_start_time,day_end_time,doctor_rating,doctor_status,created_dt,created_by,updated_dt,updated_by) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,unix_timestamp(now()),1,unix_timestamp(now()),1)`
let {results} = await writeQuery(query,[doctor_password,doctor_name,doctor_gender,doctor_phone,doctor_city,county_code,doctor_lat,doctor_long,doctor_pincode,doctor_registration_number,doctor_specialization,day_start_time,day_end_time,doctor_rating,doctor_status])
let {insertId} = results;
let token = jwt.sign({ role: 'doctor',doctor_id:insertId,doctor_phone:doctor_phone,doctor_password:doctor_password },secretKey,{ expiresIn: 60*60*24 });
let update_doctor_token = `update doctors set jwt_token = ? where id = ?;`
let update_response = await writeQuery(update_doctor_token,[token,insertId])
if(update_response){
    return({
        status:true,
        doctor_id:insertId
    })
}else{
    return({
        status:false,
        doctor_id:null
    })
}

    }catch(e){
        throw(e)
    }
}




let patientRegistration=async(opts)=>{
    try{
    let patient_phone = opts.phone;
    let patient_name = opts.patient_name;
    let patient_city = opts.patient_city;
    let patient_password = opts.patient_password;
    let county_code = opts.county_code;
    let patient_lat = opts.patient_lat;
    let patient_long = opts.patient_long;
    let patient_pincode = opts.patient_pincode;
    let patient_age = opts.patient_age;
    let patient_gender = opts.patient_gender;
    let patient_height = opts.patient_height;
    let patient_weight = opts.patient_weight;
    let patient_status = opts.patient_status;

    let query = `insert into patients (patient_name,patient_password,patient_phone,patient_city,county_code,patient_lat,patient_long,patient_pincode,patient_age,patient_gender,patient_height,patient_weight,patient_status,created_dt,created_by,updated_dt,updated_by) values (?,?,?,?,?,?,?,?,?,?,?,?,?,unix_timestamp(now()),1,unix_timestamp(now()),1)`
    let {results} = await writeQuery(query,[patient_name,patient_password,patient_phone,patient_city,county_code,patient_lat,patient_long,patient_pincode,patient_age,patient_gender,patient_height,patient_weight,patient_status])
    let {insertId} = results;
    let token = jwt.sign({ role: 'patient',patient_id:insertId,patient_phone:patient_phone },secretKey,{ expiresIn: 60*60*24 });
    let update_patient_token = `update patients set jwt_token = ? where id = ?;`
    let update_response = await writeQuery(update_patient_token,[token,insertId])
    if(update_response){
        return({
            status:true,
            patient_id:insertId
        })
    }else{
        return({
            status:false,
            patient_id:null
        })
    }
    
        }catch(e){
            throw(e)
        }
    }
    
    let assistantRegistration=async(opts)=>{
        try{
        let assistant_phone = opts.phone;
        let assistant_name = opts.assistant_name;
        let assistant_gender = opts.assistant_gender;
        let assistant_age = opts.assistant_age;
        let assistant_password = opts.assistant_password;
        let assistant_city = opts.assistant_city;
        let county_code = opts.county_code;
        let assistant_lat = opts.assistant_lat;
        let assistant_long = opts.assistant_long;
        let assistant_pincode = opts.assistant_pincode;
        let assistant_specialization = opts.assistant_specialization;
        let assistant_registration_number = opts.assistant_registration_number;
        let assistant_rating = opts.assistant_rating;
        let assistant_status = opts.assistant_status;

        let query = `insert into assistants (assistant_phone,assistant_age,assistant_password,assistant_name,assistant_gender,assistant_city,county_code,assistant_lat,assistant_long,assistant_pincode,assistant_specialization,assistant_registration_number,assistant_rating,assistant_status,created_dt,created_by,updated_dt,updated_by) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,unix_timestamp(now()),1,unix_timestamp(now()),1)`
        let {results} = await writeQuery(query,[assistant_phone,assistant_age,assistant_password,assistant_name,assistant_gender,assistant_city,county_code,assistant_lat,assistant_long,assistant_pincode,assistant_specialization,assistant_registration_number,assistant_rating,assistant_status])
        let {insertId} = results;
        let token = jwt.sign({ role: 'assistant',assistant_id:insertId,assistant_phone:assistant_phone },secretKey,{ expiresIn: 60*60*24 });
        let update_assistant_token = `update assistants set jwt_token = ? where id = ?;`
        let update_response = await writeQuery(update_assistant_token,[token,insertId])
        if(update_response){
            return({
                status:true,
                assistant_id:insertId
            })
        }else{
            return({
                status:false,
                assistant_id:null
            })
        }
        
            }catch(e){
                throw(e)
            }
        }
        
        let adminRegistration=async(opts)=>{
            try{
            let admin_phone = opts.phone;
            let admin_name = opts.admin_name;
            let admin_gender = opts.admin_gender;
            let admin_age = opts.admin_age;
            let admin_password = opts.admin_password;
            let admin_city = opts.admin_city;
            let county_code = opts.county_code;
            let admin_lat = opts.admin_lat;
            let admin_long = opts.admin_long;
            let admin_pincode = opts.admin_pincode;
            let admin_specialization = opts.admin_specialization;
            let admin_status = opts.admin_status;
    
            let query = `insert into admins (admin_phone,admin_age,admin_password,admin_name,admin_gender,admin_city,county_code,admin_lat,admin_long,admin_pincode,admin_specialization,admin_status,created_dt,created_by,updated_dt,updated_by) values (?,?,?,?,?,?,?,?,?,?,?,?,unix_timestamp(now()),1,unix_timestamp(now()),1)`
            let {results} = await writeQuery(query,[admin_phone,admin_age,admin_password,admin_name,admin_gender,admin_city,county_code,admin_lat,admin_long,admin_pincode,admin_specialization,admin_status])
            let {insertId} = results;
            let token = jwt.sign({ role: 'admin',admin_id:insertId,admin_phone:admin_phone },secretKey,{ expiresIn: 60*60*24 });
            let update_admin_token = `update admins set jwt_token = ? where id = ?;`
            let update_response = await writeQuery(update_admin_token,[token,insertId])
            if(update_response){
                return({
                    status:true,
                    admin_id:insertId
                })
            }else{
                return({
                    status:false,
                    admin_id:null
                })
            }
            
                }catch(e){
                    throw(e)
                }
            }
            

module.exports={
    checkIfDoctorAlreadyRegistered,
    checkIfAssistantAlreadyRegistered,
    checkIfPatientAlreadyRegistered,
    checkIfAdminAlreadyRegistered,
    doctorRegistration,
    patientRegistration,
    assistantRegistration,
    adminRegistration
}


// let user = await findUserbyPhone(opts);

  
  


