
const { Forbidden } = require('../../errorClasses/errorClass');
const { readQuery,writeQuery } = require('../../queryExecution/prodQueryExecution');

let patientRequestService = async(opts,patient_id,operator,role)=>{
    try{
    let disease_id = opts.disease_id;
    let clinic_id = opts.clinic_id;
    let time_slot = opts.time_slot;
    let patient_reports = opts.patient_reports;
    let query = `insert into patient_requests(patient_id,disease_id,clinic_id,expected_time_slot,request_status,patient_reports,created_dt,created_by,updated_dt,updated_by,updated_by_designation)values(?,?,?,?,'Pending',?,unix_timestamp(now()),?,unix_timestamp(now()),?,?); `
    
    let {results} = await writeQuery(query,[patient_id,disease_id,clinic_id,time_slot,patient_reports,operator,operator,role])
    let {insertId} = results;
    if(insertId){
        return({
            status:true,
            request_id:insertId
        })
    }else{
        return({
            status:false,
            request_id:null
        })
    
    }
    }catch(e){
        throw(e)
    }
    }

    let updatePatientReportRequest = async(id,reports,operator,role)=>{
        try{
            let query = `update patient_requests set patient_reports = ?,updated_dt=unix_timestamp(now()),updated_by=?,updated_by_designation=? where id =? ;`
            await writeQuery(query,[reports,operator,role,id])
            return;

        }catch(e){
            throw(e)
        }
    }
    
    module.exports={
        patientRequestService,
        updatePatientReportRequest 
    }