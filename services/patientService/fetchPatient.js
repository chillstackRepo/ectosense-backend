let {readQuery}=require("../../queryExecution/prodQueryExecution")
let getPatientDetailsService = async(patient_id)=>{
    try{
        let query = `select id,county_code,patient_phone,patient_name,patient_age,patient_gender,patient_height,patient_weight,patient_city,patient_lat,patient_long,patient_status,created_dt from patients where id = ?;`
        let details = await readQuery(query,[patient_id])
        return details;        
    }catch(e){
        throw(e)
    }
}

module.exports={getPatientDetailsService}