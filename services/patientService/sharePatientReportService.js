const { readQuery } = require("../../queryExecution/prodQueryExecution")
let {sendEmail} = require("../emailService/mailjetService")
let sharePatientReportService = async(patient_request_id,clinic_id,doctor_id)=>{
    try{
        let clinic_mail_query = `select clinic_name,clinic_email from clinics where id =?`
        let doctor_mail_query = `select doctor_name,doctor_email from doctors where id = ?`
        let clinic_mail = (await readQuery(clinic_mail_query,[clinic_id])).pop();
        let doctor_mail = (await readQuery(doctor_mail_query,[doctor_id])).pop();
        let {clinic_name,clinic_email} = clinic_mail;
        let {doctor_email,doctor_name} = doctor_mail;
        sendEmail(patient_request_id,clinic_email,clinic_name,doctor_email,doctor_name,function(err,response){
            if(!err){
                return response;    
            }
            else{
                throw(e)
            }
        });

    }
catch(e){
    throw(e)
}}

module.exports={
    sharePatientReportService
}