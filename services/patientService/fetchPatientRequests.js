let {readQuery} = require("../../queryExecution/prodQueryExecution")
let getPatientRequestService = async(patient_id)=>{
    try{
        let query = `select * from patient_requests where patient_id = ? and request_status='Pending';`
        let patient_request_details = await readQuery(query,[patient_id])
        return patient_request_details;
    }catch(e){
        throw(e)
    }
}

module.exports={
    getPatientRequestService
}