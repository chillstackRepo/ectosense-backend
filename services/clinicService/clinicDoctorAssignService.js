const {writeQuery, readQuery} = require("../../queryExecution/prodQueryExecution");
const { Forbidden } = require("../../errorClasses/errorClass");
let assignDoctorToClinicService = async(opts,operator,role)=>{
let clinic_id = opts.clinic_id;
let doctor_id = opts.doctor_id;
let relation_status = opts.relation_status;
let clinic_query = `select id,clinic_name from clinics where id =?;`
let doctor_query = `select id,doctor_name from doctors where id =?;`
let clinic_details = await readQuery(clinic_query,[clinic_id])
let doctor_details = await readQuery(doctor_query,[doctor_id])

if(clinic_details.length && doctor_details.length){
let query = `insert into clinic_doctor_relationship(clinic_id,doctor_id,relation_status,created_by,created_dt,updated_by,updated_dt,updated_by_designation) VALUES(?,?,?,?,unix_timestamp(now()),?,unix_timestamp(now()),?)`
let {results} = await writeQuery(query,[clinic_id,doctor_id,relation_status,operator,operator,role])
let {insertId} = results;
if(insertId)
    return true;
else    
    return false;
}
else{
throw new Forbidden("Please check with cluster manegor to get clear picture of Issue.")
}
}
module.exports={
    assignDoctorToClinicService
}