const {writeQuery, readQuery} = require("../../queryExecution/prodQueryExecution");
const { Forbidden } = require("../../errorClasses/errorClass");
let assignAssistantToDoctorService = async(opts,operator,role)=>{
let doctor_id = opts.doctor_id;
let assistant_id = opts.assistant_id;
let relation_status = opts.relation_status;
let doctor_query = `select id,doctor_name from doctors where id =?;`
let assistant_query = `select id,assistant_name from assistants where id =?;`
let doctor_details = await readQuery(doctor_query,[doctor_id])
let assistant_details = await readQuery(assistant_query,[assistant_id])
if(doctor_details.length && assistant_details.length){
let query = `insert into doctor_assistant_relationship(doctor_id,assistant_id,relation_status,created_by,created_dt,updated_by,updated_dt,updated_by_designation) VALUES(?,?,?,?,unix_timestamp(now()),?,unix_timestamp(now()),?)`
let {results} = await writeQuery(query,[doctor_id,assistant_id,relation_status,operator,operator,role])
let {insertId} = results;
if(insertId)
    return true;
else    
    return false;
}
else{
throw new Forbidden("Please check with cluster manager to get clear picture of Issue.")
}
}
module.exports={
    assignAssistantToDoctorService
}