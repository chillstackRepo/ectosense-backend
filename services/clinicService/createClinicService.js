const h3 = require("h3-js");
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');

const {writeQuery, readQuery}=require("../../queryExecution/prodQueryExecution")
let clinicCreationService = async(opts,operator,role)=>{
try{
        let lat = opts.clinic_lat;
        let long = opts.clinic_long;
        let resolution = opts.resolution
        let h3_index = h3.geoToH3(lat,long,resolution);
        console.log(h3_index)
        let query = `select * from clusters where h3_index=?`
        let cluster = await readQuery(query,[h3_index])
        if(cluster.length){
            let cluster_details = cluster.pop();
            let cluster_id = cluster_details.id ;
            let clinic_name = opts.clinic_name;
            let clinic_registration_number = opts.clinic_registration_number;
            let clinic_start_time = opts.clinic_start_time;
            let clinic_end_time = opts.clinic_end_time;
            let clinic_cluster_id = cluster_id;
            let clinic_pincode = opts.clinic_pincode;

            let query = `insert into clinics(clinic_name,clinic_registration_number,clinic_start_time,clinic_end_time,clinic_cluster_id,clinic_pincode,created_by,created_dt,updated_by,updated_dt,updated_by_designation) VALUES(?,?,?,?,?,?,1,unix_timestamp(now()),?,unix_timestamp(now()),?);`
            let {results} = await writeQuery(query,[clinic_name,clinic_registration_number,clinic_start_time,clinic_end_time,clinic_cluster_id,clinic_pincode,operator,role])
            let {insertId} = results;
            return {clinic_id:insertId,cluster_id:cluster_id}
    

        }else{
            throw(new Forbidden(`Sorry!! You can not create clinic outside clusters.`));

        }
}catch(e){
    throw(e)
}
}
let checkPreviousClinics = async(opts)=>{
    try{
            let clinic_name = opts.clinic_name
            let query = `select * from clinics where clinic_name=?`
            let results = await readQuery(query,[clinic_name])
            if(results.length){
                let clinic_details = results.pop()  
                return {isAlreadyPresent:true,clinic:clinic_details.id}
                }
            else
                return {isAlreadyPresent:false}
    }catch(e){
        throw(e)
    }
    }
    

module.exports={
    clinicCreationService,
    checkPreviousClinics
}