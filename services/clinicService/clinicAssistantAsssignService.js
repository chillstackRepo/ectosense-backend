const {writeQuery, readQuery} = require("../../queryExecution/prodQueryExecution");
const { Forbidden } = require("../../errorClasses/errorClass");
let assignAssistantToClinicService = async(opts,admin)=>{
let clinic_id = opts.clinic_id;
let assistant_id = opts.assistant_id;
let relation_status = opts.relation_status;
let clinic_query = `select id,clinic_name from clinics where id =?;`
let assistant_query = `select id,assistant_name from assistants where id =?;`
let clinic_details = await readQuery(clinic_query,[clinic_id])
let assistant_details = await readQuery(assistant_query,[assistant_id])

if(clinic_details.length && assistant_details.length){
let query = `insert into clinic_assistant_relationship(clinic_id,assistant_id,relation_status,created_by,created_dt,updated_by,updated_dt) VALUES(?,?,?,?,unix_timestamp(now()),?,unix_timestamp(now()))`
let {results} = await writeQuery(query,[clinic_id,assistant_id,relation_status,admin,admin])
let {insertId} = results;
if(insertId)
    return true;
else    
    return false;
}
else{
throw new Forbidden("Please check with cluster manager to get clear picture of Issue.")
}
}
module.exports={
    assignAssistantToClinicService
}