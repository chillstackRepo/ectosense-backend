const { readQuery } = require("../../queryExecution/prodQueryExecution");
let TransactionQueryService = require("../../queryExecution/transactionQuery");

let createDoctorRequestAndChangeStatus = async(doctor_id,assistant_request_id,selected_time_slot,operator,role)=>{
    try{
        console.log(doctor_id,assistant_request_id,selected_time_slot,operator,role)
        let query = [];
        let request_details ='select * from assistant_requests where id = ?;'
        let assistant_request_details = (await readQuery(request_details,[assistant_request_id])).pop();
        let patient_request_id = assistant_request_details.patient_request_id;
       
        query.push(`insert into doctor_requests (doctor_id,patient_request_id,assistant_request_id,time_slot_id,request_status,created_dt,created_by,updated_dt,updated_by,updated_by_designation) VALUES (${doctor_id},${patient_request_id},${assistant_request_id},${selected_time_slot},'Pending',unix_timestamp(now()),${operator},unix_timestamp(now()),${operator},'${role}')`);
        query.push(`update assistant_requests set request_status='Processing',updated_dt=unix_timestamp(now()),updated_by=${operator},updated_by_designation='${role}' where id = ${assistant_request_id}`)
     
        query.push(`update doctor_schedules set time_slot_status=1,updated_dt=unix_timestamp(now()),updated_by=${operator},updated_by_designation='${role}' where doctor_id = ${doctor_id} and time_slot_id=${selected_time_slot}`)
     
       query.push(`update patient_requests set assigned_time_slot = ${selected_time_slot}, updated_dt=unix_timestamp(now()),updated_by_designation='${role}' where id=${patient_request_id}`)
       
       let [doctor_requests] = await TransactionQueryService.multiQuery(query);
       return({
        doctor_request_id:doctor_requests.insertID
       })

       
    }catch(e){
    throw(e)
    }
}

module.exports={
    createDoctorRequestAndChangeStatus
}