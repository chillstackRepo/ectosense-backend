let {readQuery} = require("../../queryExecution/prodQueryExecution")
let selectDoctorListService = async(assistant_id,patient_request_id)=>{
    try{
        let query = `select * from patient_requests where id = ?`
        let patient_details = (await readQuery(query,[patient_request_id])).pop();
        let patient_disease = patient_details.disease_id;
        let query_doctorSelection = `select id,doctor_name,county_code,doctor_phone,doctor_gender,doctor_specialization,doctor_fee from doctors where id in(
            select ddm.doctor_id from doctor_assistant_relationship dar,doctor_disease_map ddm where dar.doctor_id = ddm.doctor_id 
            and dar.assistant_id =? and ddm.disease_id=?); `
        let doctorList = await readQuery(query_doctorSelection,[assistant_id,patient_disease]);
        return doctorList;


    }catch(e){
        throw(e)
    }
}

module.exports={
    selectDoctorListService
}