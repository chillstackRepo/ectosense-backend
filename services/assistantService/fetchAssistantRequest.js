let {readQuery} = require("../../queryExecution/prodQueryExecution")
let getAssistantRequestService = async(assistant_id)=>{
    try{
        let query = `select * from assistant_requests where assistant_id = ? and request_status='Pending';`
        let assistant_request_details = await readQuery(query,[assistant_id])
        return assistant_request_details;
    }catch(e){
        throw(e)
    }
}

module.exports={
    getAssistantRequestService
}