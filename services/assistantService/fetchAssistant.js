let {readQuery}=require("../../queryExecution/prodQueryExecution")
let getAssistantDetailsService = async(assistant_id)=>{
    try{
        let query = `select id,assistant_name,county_code,assistant_phone,assistant_gender,assistant_age,assistant_specialization,assistant_city,assistant_status,assistant_lat,assistant_long,assistant_pincode,assistant_rating,created_dt from assistants where id = ?;`
        let details = await readQuery(query,[assistant_id])
        return details;        
    }catch(e){
        throw(e)
    }
}

module.exports={getAssistantDetailsService}