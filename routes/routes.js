
const express = require('express');
const {protect} = require("../controllers/authController/authController")
const {registrationtController} = require("../controllers/authController/registrationController")
const {loginController} = require("../controllers/authController/loginController")
const {patientRequestController} = require("../controllers/patientController/patientRequestController")
const {addDisease}=require("../controllers/adminController/diseaseController")
const {createClusterController}=require("../controllers/h3Controller/createCluster")
const {createClinicsController}=require("../controllers/clinicController/clinicController")
const {patientUploadController}=require("../controllers/patientController/patientUploadController")
const {doctorUploadController}=require("../controllers/doctorController/doctorUploadController")
const {patientDownloadController}=require("../controllers/patientController/patientDownloadController")
const {appointClinicDoctorController}=require("../controllers/clinicController/appointClinicDoctor")
const {appointClinicAssistantController}=require("../controllers/clinicController/appointClinicAssistant")
const {assignAssistantToDoctorController}=require("../controllers/clinicController/assignDoctorAssistant")
const {attachDoctorDiagnosisDiseaseController}=require("../controllers/doctorController/doctorDiagnosisDisease")
const {doctorListForPatientController}=require("../controllers/assistantController/assistantController")
const {getDoctorDetails}=require("../controllers/doctorController/doctorDetails")
const {getPatientDetails}=require("../controllers/patientController/patientDetails")
const {getAssistantDetails}=require("../controllers/assistantController/assistantDetails")
const {getAssistantRequests} = require("../controllers/assistantController/assistantRequest")
const {getDoctorRequests} = require("../controllers/doctorController/doctorRequests")
const {getPatientRequests} = require("../controllers/patientController/patientRequests")
const {assignDoctorToPatientController} = require("../controllers/assistantController/doctorAssignment")
const {doctorschedulesController} = require("../controllers/doctorController/doctorSchedules")
const {postDiagnosisController} = require("../controllers/doctorController/postDiagnosis")
const {patientDiagnosisReportController}=require("../controllers/patientController/patientDiagnosisReportController")
const {apidocController}=require("../controllers/apidocController/apidocController")
var router = express.Router();

// Protect all routes after this middleware
router.route('/api-doc').get(apidocController);//assistant,doctor,admin
router.route('/registration').post(registrationtController);
router.route('/login').post(loginController);
router.use(protect);
router.route('/create-cluster').post(createClusterController);//admin
router.route('/create-clinic').post(createClinicsController);//admin
router.route('/disease-diagnose-by-doctor').post(attachDoctorDiagnosisDiseaseController);//admin
router.route('/appoint-doctor-in-clinic').post(appointClinicDoctorController);//admin
router.route('/appoint-assistant-in-clinic').post(appointClinicAssistantController);//admin
router.route('/assign-assistant-to-doctor').post(assignAssistantToDoctorController);
router.route('/add-disease').post(addDisease);//admin
router.route('/patient-treatment-request').post(patientRequestController);//patient,assistant,doctor,admin
router.route('/patient-report-uploader').post(patientUploadController);//patient,assistant,doctor,admin
router.route('/patient-report-downloader').post(patientDownloadController);//patient,assistant,doctor,admin
router.route('/share-patient-diagnosis').post(patientDiagnosisReportController);//patient,assistant,doctor,admin
router.route('/get-doctor-list-for-assistant').post(doctorListForPatientController);//assistant,admin
router.route('/get-doctor-schedules').post(doctorschedulesController);//assistant,admin
router.route('/assign-doctor-to-patient').post(assignDoctorToPatientController);//assistant,admin
router.route('/doctor-prescription-uploader').post(doctorUploadController);//doctor
router.route('/post-doctor-diagnosis').post(postDiagnosisController);//doctor
router.route('/get-doctor-details').post(getDoctorDetails);//patient,assistant,doctor,admin
router.route('/get-patient-details').post(getPatientDetails);//patient,assistant,doctor,admin
router.route('/get-assistant-details').post(getAssistantDetails);//patient,assistant,doctor,admin
router.route('/get-patient-requests').post(getPatientRequests);//assistant,admin
router.route('/get-doctor-requests').post(getDoctorRequests);//doctor,assistant,admin
router.route('/get-assistant-requests').post(getAssistantRequests);//assistant,doctor,admin



module.exports = router;
  
