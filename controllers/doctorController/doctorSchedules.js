const {getDoctorSchedulesService}=require("../../services/doctorService/doctorSchedules")
let {validateGetDoctorSchedules} = require("../../validator/validateGetDoctorSchedules")
let {Forbidden}= require("../../errorClasses/errorClass")

/**
 * @api {post} /api/v1/get-doctor-schedules
 * @apiName Assistant/Assistant Request
 * @apiPermission ALL
 * @apiGroup Assistant-Service 
    * @apiDescription  <h2>Get doctor schedules.</h2>
    * Repository: "extosence"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} doctor_id  Mandatory : doctor_id of the patient assistant.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. successfully fetched doctor details.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Successfully fetched today's doctor Schedules.",
*    "status": 200,
*    "data": []
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let doctorschedulesController = async(req,res,next)=>{
    try{
        if(req.user.role=='admin'|| req.user.role=='assistant'){
        let opts = req.body;
        validateGetDoctorSchedules(opts)
        let doctor_id = opts.doctor_id;
        let schedules = await getDoctorSchedulesService(doctor_id);
        res.status(200).send(
            {
                message:"Successfully fetched today's doctor Schedules.",
                status:200,
                data:schedules

            }
        )
        }else{
            next(new Forbidden(res.__("errors.forbidden")));

        }
    }catch(e){
        next(e)
    }
}

module.exports={
    doctorschedulesController
}