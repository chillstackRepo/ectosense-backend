let {getDoctorDetailsService} = require("../../services/doctorService/fetchDoctors")
let {validateGetDoctorDetails} = require("../../validator/validateGetDoctorDetails")
let {Forbidden}= require("../../errorClasses/errorClass")

/**
 * @api {post} /api/v1/get-doctor-details
 * @apiName Doctor/Doctor Request
 * @apiPermission ALL
 * @apiGroup Doctor-Service 
    * @apiDescription  <h2>Get doctor all details.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} doctor_id  Mandatory : doctor_id of the doctor.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. successfully fetched doctor details.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "successfully fetched doctor requests.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */
let getDoctorDetails = async(req,res,next)=>{
    try{
        let opts = req.body;
        validateGetDoctorDetails(opts)
        let doctor_id = opts.doctor_id;
        let doctor_details = await getDoctorDetailsService(doctor_id);
        if(doctor_details.length){
            let doctor = doctor_details.pop();
            res.status(200).send({
                message:"successfully fetched doctor details.",
                status:200,
                data:doctor
            })
        }
        else{
            next(new Forbidden(res.__("No such doctor exists.")));

        }
    }catch(e){
        next(e)
    }
}

module.exports={
    getDoctorDetails
}