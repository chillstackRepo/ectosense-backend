const {upload} = require('../../services/uploadService/s3Uploader');
const {uploadDoctorPrescriptionService} = require('../../services/doctorService/uploadDoctorPrescription')
const singleUpload = upload.single('image');
const {validateDoctorUpload} = require("../../validator/validateDoctorUpload")
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');

/**
 * @api {post} /api/v1/doctor-prescription-uploader
 * @apiName Doctor/Doctor Request
 * @apiPermission Doctor
 * @apiGroup Doctor-Service 
    * @apiDescription  <h2>Upload prescription by doctor.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} patient_request_id  Mandatory : patient_request_id of the patient.<br>
  * @apiParam {String} file  Mandatory : prescription of the patient.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Prescription successfully uploaded.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Prescription successfully uploaded.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let doctorUploadController = async(req,res,next)=>{
        if(req.user.role=='admin'){
        singleUpload(req, res, async function(err) {
            try{
            let operator = req.user.registration_id;
            let role = req.user.role;
            let opts =req.body;
            validateDoctorUpload(opts,req);
            let patient_request_id = opts.patient_request_id;

            let fileURL = (req.file.location).toString();

            if (err) {
              return res.status(422).send({errors: [{title: 'File Upload Error', detail: err.message}] });
            }
            
            await uploadDoctorPrescriptionService(patient_request_id,fileURL,operator,role);
            res.status(200).send({
                message:`Prescription successfully uploaded .`,
                status:200,
                data: {'imageUrl': req.file.location}
              })
            }catch(e){
                next(new InternalServerError(res.__("errors.genericMessage")))
            }
          });
          
       
        }else{
            next(new Forbidden(res.__("errors.forbidden")));

        } 
   
}

module.exports={doctorUploadController}


