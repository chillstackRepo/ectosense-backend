
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');
const{attachDoctorDiagnosisDiseaseService}= require("../../services/doctorService/doctorDiagnosisDiseaseService")
const {validateAttachDoctorDiagnosisDisease}=require("../../validator/validateAttachDoctorDiagnosisDisease")
/**
 * @api {post} /api/v1/disease-diagnose-by-doctor
 * @apiName Doctor/Doctor Onboarding
 * @apiPermission ADMIN
 * @apiGroup Doctor-Service 
    * @apiDescription  <h2>Attach disease treatmentwith Doctor.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} doctor_id  Mandatory : Registered Doctor ID.<br>
  * @apiParam {String} disease_id  Mandatory : Registered Disease Treatments ID.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Dr. Meena is successfully registered for diagnose Heart Disease.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Reports successfully Downloaded.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let attachDoctorDiagnosisDiseaseController = async(req,res,next)=>{
    try{
        if(req.user.role=="admin"){
        let operator = req.user.registration_id;
        let role = req.user.role;
        let opts = req.body;
        validateAttachDoctorDiagnosisDisease(opts)
        let {status,doctor_name,disease_name}  = await attachDoctorDiagnosisDiseaseService(opts,operator,role)
        if(status){
            res.status(200).send({
                message:`Dr. ${doctor_name} is successfully registered for diagnose ${disease_name}.`,
                status:200,
                data:{disease_name:disease_name,doctor_name:doctor_name,admin:operator}
            })
        }else{
            next(new InternalServerError(res.__("errors.genericMessage")))
        }
        }else{
            next(new Forbidden(res.__("errors.forbidden")));
        }


    }catch(e){
        next(new InternalServerError(res.__("errors.genericMessage")))
    }
}

module.exports={
    attachDoctorDiagnosisDiseaseController
}