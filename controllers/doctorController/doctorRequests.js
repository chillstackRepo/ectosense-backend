let {getDoctorRequestService}=require("../../services/doctorService/fetchDoctorRequests")
let {validateGetDoctorRequests} = require("../../validator/validateGetDoctorRequests")
/**
 * @api {post} /api/v1/get-doctor-requests
 * @apiName Doctor/Doctor Request
 * @apiPermission Doctors
 * @apiGroup Doctor-Service 
    * @apiDescription  <h2>Get doctor all requests.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} doctor_id  Mandatory : doctor_id of the assistant.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. successfully fetched doctor requests..
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "successfully fetched doctor requests.",
*    "status": 200,
*    "data": []
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */
let getDoctorRequests = async(req,res,next)=>{
    try{
        let opts = req.body;
        validateGetDoctorRequests(opts)
        let doctor_id = opts.doctor_id;
        let doctor_request_details = await getDoctorRequestService(doctor_id);
            res.status(200).send({
                message:"successfully fetched doctor requests.",
                status:200,
                data:doctor_request_details
            })
        

    }catch(e){
        next(e)
    }
}
module.exports={
    getDoctorRequests
}