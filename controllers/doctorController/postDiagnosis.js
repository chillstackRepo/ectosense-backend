const {postDiagnosisService} = require("../../services/doctorService/postDiagnosisService");
const {validatePostDiagnosis}= require("../../validator/validatePostDiagnosis")
/**
 * @api {post} /api/v1/post-doctor-diagnosis
 * @apiName Doctor/Doctor Request
 * @apiPermission Doctor
 * @apiGroup Doctor-Service 
    * @apiDescription  <h2>Complete case by doctor.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} doctor_request_id  Mandatory : doctor_request_id of the doctor.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Prescription successfully uploaded.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Prescription successfully uploaded.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let postDiagnosisController = async(req,res,next)=>{
    try{
        let opts = req.body;
        validatePostDiagnosis(opts)
        let doctor_request_id = opts.doctor_request_id;
        let operator = req.user.registration_id;
        let role = req.user.role;

        let results = await postDiagnosisService(doctor_request_id,operator,role);  
        let {patient_request_id,already_processed,doctor_id} = results; 
        if(already_processed){
            res.status(200).send({
                message:`Patient request ${patient_request_id} already dianosed by Dr.${doctor_id}.`,
                status:200,
                data:results
            })
    
        }else{
        res.status(200).send({
            message:`Congrats , You have been successfully diagnosed patient request ${patient_request_id}.`,
            status:200,
            data:results
        })
    }

    }catch(e){
        next(e)
    }
}

module.exports={
    postDiagnosisController
}