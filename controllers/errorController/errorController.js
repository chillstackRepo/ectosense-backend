// const cloudWatch = require("../../services/notificationServices/cloudwatchService");
const internalIp = require("internal-ip");
const internalIPAddress = internalIp.v4.sync() ? internalIp.v4.sync() : "NULL";
const {
  InternalServerError,
} = require("../../errorClasses/errorClass");

module.exports = (err, req, res, next) => {
  let errorObject = {
      message: err.message,
      name: err.name,
      status: err.status,
      data:null
  };
  console.log(err)
  // cloudWatch.error(
  //   `Requested ${req.method} ${req.originalUrl}`,
  //   "userId",
  //   errorObject,
  //   internalIPAddress,
  //   req.body,
  //   err.stack
  // );

  if (!err.status) err = new InternalServerError(res.__('errors.genericMessage'));

  return res.status(err.status).json(errorObject);
  
};
