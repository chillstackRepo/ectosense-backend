
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');
const{assignAssistantToClinicService}= require("../../services/clinicService/clinicAssistantAsssignService")
const {validateAppointClinicAssistant}=require("../../validator/validateAppointClinicAssistant")
/**
 * @api {post} /api/v1/appoint-assistant-in-clinic
 * @apiName Assistant/Assistant Appointing
 * @apiPermission ADMIN
 * @apiGroup Assistant-Service 
    * @apiDescription  <h2>Attach Assistant with clinic.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} assistant_id  Mandatory : Registered Assistant ID.<br>
  * @apiParam {String} clinic_id  Mandatory : Registered Clinic ID.<br>
  * @apiParam {String} relation_status  Mandatory : active/inactive.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Assistant successfully assigned with clinic 5.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Assistant successfully assigned with clinic 5.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let appointClinicAssistantController = async(req,res,next)=>{
    try{
        if(req.user.role=="admin"){
        let admin = req.user.admin_id;
        let opts = req.body;
        validateAppointClinicAssistant(opts)
        let results  = await assignAssistantToClinicService(opts,admin)
        if(results){
            res.status(200).send({
                message:`Assistant successfully assigned with clinic ${opts.clinic_id}.`,
                status:200,
                data:{assistant:opts.assistant_id,clinic:opts.clinic_id,admin:admin}
            })
        }else{
            next(new InternalServerError(res.__("errors.genericMessage")))
        }
        }else{
            next(new Forbidden(res.__("errors.forbidden")));
        }


    }catch(e){
        // next(new InternalServerError(res.__("errors.genericMessage")))

        next(e)
    }
}

module.exports={
    appointClinicAssistantController
}