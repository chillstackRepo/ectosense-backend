const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');
const {clinicCreationService,checkPreviousClinics} = require("../../services/clinicService/createClinicService")
const {validateCreateClinic} = require("../../validator/validateCreateClinic")
/**
 * @api {post} /api/v1/create-clinic
 * @apiName Clinic/Create Clinic
 * @apiPermission ADMIN
 * @apiGroup Clinic-Service 
    * @apiDescription  <h2>Create a new Clinic.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} clinic_lat  Mandatory e.<br>
  * @apiParam {String} clinic_long  Mandatory <br>
  * @apiParam {String} resolution  Mandatory <br>
  * @apiParam {String} clinic_name  Mandatory <br>
  * @apiParam {String} county_code  Mandatory <br>
  * @apiParam {String} clinic_phone  Mandatory <br>
  * @apiParam {String} clinic_email  Mandatory <br>
  * @apiParam {String} clinic_registration_number  Mandatory <br>
  * @apiParam {String} clinic_start_time  Mandatory <br>
  * @apiParam {String} clinic_end_time  Mandatory <br>
  * @apiParam {String} clinic_pincode  Mandatory <br>

  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Successfully created Heart Care clinic in cluster 5.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Successfully created Heart Care clinic in cluster 5.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let createClinicsController = async(req,res,next)=>{
    try{
   if(req.user.role=="admin"){
    let opts = req.body;
    validateCreateClinic(opts)
    let operator = req.user.registration_id;
    let role = req.user.role;

    let {isAlreadyPresent,clinic} = await checkPreviousClinics(opts)
if(!isAlreadyPresent){
    let {clinic_id,cluster_id} = await clinicCreationService(opts,operator,role)
    res.status(200).send({
        message:`Successfully created  ${opts.clinic_name} clinic in cluster ${cluster_id}.`,
        status:200,
        data: {
            clinic_id:clinic_id,
            cluster_id:cluster_id}
      })
    }else if(isAlreadyPresent){
        next(new Forbidden(`clinic already present with id ${clinic}.`));

    }
    else{
        next(new Forbidden(res.__("errors.forbidden")));
    
    }
         
   }else{
    next(new Forbidden(res.__("errors.forbidden")));
   }    
    }catch(e){       
        // next(e)
        next(new InternalServerError(res.__("errors.genericMessage")));

    }

}

module.exports={
    createClinicsController
}




