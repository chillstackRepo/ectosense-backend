
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');
const{assignDoctorToClinicService}= require("../../services/clinicService/clinicDoctorAssignService")
const {validateAppointClinicDoctor} = require("../../validator/validateAppointClinicDoctor")
/**
 * @api {post} /api/v1/appoint-doctor-in-clinic
 * @apiName Doctor/Doctor Appointing
 * @apiPermission ADMIN
 * @apiGroup Doctor-Service 
    * @apiDescription  <h2>Attach doctor with clinic.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} doctor_id  Mandatory : Registered Doctor ID.<br>
  * @apiParam {String} clinic_id  Mandatory : Registered Clinic ID.<br>
  * @apiParam {String} relation_status  Mandatory : active/inactive.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Doctor successfully assigned with clinic 5.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Doctor successfully assigned with clinic 5.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let appointClinicDoctorController = async(req,res,next)=>{
    try{
        if(req.user.role=="admin"){
        let operator = req.user.registration_id;
        let role = req.user.role;
        let opts = req.body;
        validateAppointClinicDoctor(opts)
        let results  = await assignDoctorToClinicService(opts,operator,role)
        if(results){
            res.status(200).send({
                message:`Doctor successfully assigned with clinic ${opts.clinic_id}.`,
                status:200,
                data:{doctor:opts.doctor_id,clinic:opts.clinic_id,admin:operator}
            })
        }else{
            // next(new InternalServerError(res.__("errors.genericMessage")))
            next(e)

        }
        }else{
            next(new Forbidden(res.__("errors.forbidden")));
        }


    }catch(e){
        // next(new InternalServerError(res.__("errors.genericMessage")))

        next(e)
    }
}

module.exports={
    appointClinicDoctorController
}