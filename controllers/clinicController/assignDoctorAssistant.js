
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');
const{assignAssistantToDoctorService}= require("../../services/clinicService/doctorAssistantAssignService")
const {validateAssignAssistantToDoctor} = require("../../validator/validateAssignAssistantToDoctor")
/**
 * @api {post} /api/v1/assign-assistant-to-doctor
 * @apiName Assistant/Assistant Assignment
 * @apiPermission ADMIN
 * @apiGroup Assistant-Service 
    * @apiDescription  <h2>Attach Assistant with clinic.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} assistant_id  Mandatory : Registered Assistant ID.<br>
  * @apiParam {String} doctor_id  Mandatory : Registered Doctor ID.<br>
  * @apiParam {String} relation_status  Mandatory : active/inactive.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Assistant successfully assigned with doctor 5.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Assistant successfully assigned with doctor 5.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let assignAssistantToDoctorController = async(req,res,next)=>{
    try{
        if(req.user.role=="admin"){
            let operator = req.user.registration_id;
            let role = req.user.role;
            let opts = req.body;
            validateAssignAssistantToDoctor(opts)
        let results  = await assignAssistantToDoctorService(opts,operator,role)
        if(results){
            res.status(200).send({
                message:`assistant successfully assigned with doctor ${opts.doctor_id}.`,
                status:200,
                data:{assistant:opts.assistant_id,doctor_id:opts.doctor_id,admin:operator}
            })
        }else{
            next(new InternalServerError(res.__("errors.genericMessage")))
        }
        }else{
            next(new Forbidden(res.__("errors.forbidden")));
        }


    }catch(e){
        // next(new InternalServerError(res.__("errors.genericMessage")))

        next(e)
    }
}

module.exports={
    assignAssistantToDoctorController
}