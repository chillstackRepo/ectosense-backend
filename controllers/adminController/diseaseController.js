const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');
const {writeQuery}=require("../../queryExecution/prodQueryExecution")
let {validateAddDisease}=require("../../validator/validateAddDisease")
/**
 * @api {post} /api/v1/add-disease
 * @apiName Doctor/Add disease
 * @apiPermission ADMIN
 * @apiGroup Doctor-Service 
    * @apiDescription  <h2>Attach Disease in treatment list.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} disease_name  Mandatory : Disease Name for identification.<br>
  * @apiParam {String} critical_level  Mandatory : Disease Criticality.<br>
  * @apiParam {String} test_required  Mandatory : 1/0.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Successfully Added Disease category named Kidney Disease.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Successfully Added Disease category named Kidney Disease.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let addDisease = async(req,res,next)=>{
try{
if(req.user.role=='admin'){
let opts = req.body;    
validateAddDisease(opts);
let operator = req.user.registration_id;
let role = req.user.role;
let disease_name = opts.disease_name;
let critical_level = opts.critical_level;
let test_required = opts.test_required;

let query = `insert into diseases(disease_name,critical_level,test_required,created_by,created_dt,updated_by,updated_dt,updated_by_designation) VALUES(?,?,?,?,unix_timestamp(now()),?,unix_timestamp(now()),?)`
let {results} = await writeQuery(query,[disease_name,critical_level,test_required,operator,operator,role])
let {insertId} = results;
if(insertId){
res.status(200).send({
    status:200,
    data:{
        message:`Successfully Added Disease category named ${disease_name}`,
        disease_id:insertId
    }  
  })

}
else{
    next(new Forbidden(res.__("errors.forbidden")));

}
}
}catch(e){
    // next(new InternalServerError(res.__("errors.genericMessage")));
    next(e)
}
}

module.exports={
    addDisease
}