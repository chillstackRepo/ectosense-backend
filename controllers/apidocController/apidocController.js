const path = require('path');
const open = require('open');

let apidocController = async(req,res,next)=>{
    try{
        let opts = req.query;
        await open(path.join(__dirname+'../../../apidoc/index.html'),{app: ['google chrome', '--incognito']});
        res.send("Please follow up the documentation to use ecto API's.")
    }catch(e){
        next(e)
    }
}

module.exports={apidocController}