const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');
const {clusterCreationService,checkPreviousClusters} = require("../../services/h3Services/clusterCreationService")
const {validateClusterCreation}=require("../../validator/validateClusterCreation") 

/**
 * @api {post} /api/v1/create-cluster
 * @apiName Clinic/Create Clinic
 * @apiPermission ADMIN
 * @apiGroup Clinic-Service 
    * @apiDescription  <h2>Create a new Cluster.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} lat  Mandatory : Location Lattitude.<br>
  * @apiParam {String} long  Mandatory : Location Longnitude.<br>
  * @apiParam {String} cluster_name  Mandatory : Cluster Name <br>
  * @apiParam {String} resolution  Mandatory : Uber H3 resolutions for hexagon area. <br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Successfully created Electronic City cluster with resolution level 7.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Successfully created Electronic City cluster with resolution level 7.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let createClusterController = async(req,res,next)=>{
    try{
        if(req.user.role=="admin"){
        let opts = req.body;
        validateClusterCreation(opts)
        let operator = req.user.registration_id;
        let role = req.user.role;
        let {isAlreadyPresent,cluster} = await checkPreviousClusters(opts)
    if(!isAlreadyPresent){
        let {cluster_id,cluster_name,h3_index} = await clusterCreationService(opts,operator,role)
        res.status(200).send({
            message:`Successfully created  ${cluster_name} cluster with resolution level ${opts.resolution}.`,
            status:200,
            data: {
                cluster_id:cluster_id,
                h3_index:h3_index}
          })
        }else if(isAlreadyPresent){
            next(new Forbidden(`clusters already present with id ${cluster}.`));

        }
        else{
            next(new Forbidden(res.__("errors.forbidden")));
        
        }
    }else{
        next(new Forbidden(res.__("errors.forbidden")));

    }      

    }catch(e){
        // next(e.message)
     next(new InternalServerError(res.__("errors.genericMessage")));
}
}

module.exports = {
    createClusterController
}