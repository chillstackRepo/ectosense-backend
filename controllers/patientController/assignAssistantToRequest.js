const { readQuery} = require("../../queryExecution/prodQueryExecution");
const {getAssistant} = require("./getAssistantForPatient")
let assignRequest = async(data,cb)=>{
    try{
let request_id = data.patient_request_id;
let patient_details = await readQuery(`select * from patient_requests where id = ? and request_status ='Pending'`,[request_id]);

if(patient_details.length){
    
    let {selected_assistants} = await getAssistant(patient_details,request_id)
    return cb(null,selected_assistants);
}
    }catch(e){
        throw(e)
    }
}

module.exports={
    assignRequest
}