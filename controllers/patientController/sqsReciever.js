let AWS = require('aws-sdk');
const config = require('../../config')
const{assignRequest}=require('./assignAssistantToRequest')
AWS.config.update({
  accessKeyId: config.awsConfig.awsAccessKeyId,
  secretAccessKey: config.awsConfig.awsSecretKey,
  region: config.awsConfig.awsRegion
});

let getRequestFromSQS = async()=>{
    try{
// Load the AWS SDK for Node.js
let sqs = new AWS.SQS({apiVersion: '2012-11-05'});

let queueURL = config.awsQueueConfig.QueueUrl;

let params = {
 AttributeNames: [
    "SentTimestamp"
 ],
 MaxNumberOfMessages: 10,
 MessageAttributeNames: [
    "All"
 ],
 QueueUrl: queueURL,
 VisibilityTimeout: 20,
 WaitTimeSeconds: 0
};

sqs.receiveMessage(params, function(err, data) {
  if (err) {
    console.log("Receive Error", err);
  } else if (data.Messages) {
    
    let patient_data = JSON.parse(data.Messages[0].Body)
    console.log("Message Deleted", JSON.parse(data.Messages[0].Body));
    assignRequest(patient_data,function(err,res){
        if (err) 
            console.log("Receive Error", err); 
            console.log("*****",res)
  
        if(res) {
            console.log(res)
            let deleteParams = {
            QueueUrl: queueURL,
            ReceiptHandle: data.Messages[0].ReceiptHandle
          };
          sqs.deleteMessage(deleteParams, function(err, data) {
            if (err) {
              console.log("Delete Error", err);
            } else {
              console.log("Message Deleted", data);
            }
          });
        }
    })


  }
});
    }catch(e){
        throw(e)
    }
}

getRequestFromSQS()

module.exports ={
    getRequestFromSQS
}