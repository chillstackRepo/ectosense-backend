let {sharePatientReportService}= require("../../services/patientService/sharePatientReportService") 
let {validatepatientDiagnosisReport} = require("../../validator/validatepatientDiagnosisReport")
/**
 * @api {post} /api/v1/patient-diagnosis-report-share
 * @apiName Patient/Patient Request
 * @apiPermission ALL
 * @apiGroup Patient-Service 
    * @apiDescription  <h2>Patient Report share.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} patient_request_id  Mandatory : patient_request_id of the patient.<br>
  * @apiParam {String} clinic_id  Mandatory : clinic_id of the doctor.<br>
  * @apiParam {String} doctor_id  Mandatory : doctor_id of the doctor.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Shared Successfully..
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Shared Successfully.",
*    "status": 200,
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let patientDiagnosisReportController = async(req,res,next)=>{
try{
let opts = req.body;
validatepatientDiagnosisReport(opts)
let patient_request_id = opts.patient_request_id;
let clinic_id = opts.clinic_id;
let doctor_id = opts.doctor_id;
let results = await sharePatientReportService(patient_request_id,clinic_id,doctor_id);
res.status(200).send({
    message:"Shared Successfully.",
    status : 200,
    data:results
})

}catch(e){
next(e)
}
}

module.exports={
    patientDiagnosisReportController
}