let {getPatientRequestService}=require("../../services/patientService/fetchPatientRequests")
let {validateGetPatienRequests} = require("../../validator/validateGetPatientRequests")
/**
 * @api {post} /api/v1/get-patient-requests
 * @apiName Patient/Patient Request
 * @apiPermission ALL
 * @apiGroup Patient-Service 
    * @apiDescription  <h2>Get Patient all requests.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} patient_id  Mandatory : doctor_id of the patient.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. successfully fetched patient requests..
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "successfully fetched patient requests.",
*    "status": 200,
*    "data": []
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */
let getPatientRequests = async(req,res,next)=>{
    try{
        let opts = req.body;
        validateGetPatienRequests(opts)
        let assistant_id = opts.assistant_id;
        let assistant_request_details = await getPatientRequestService(assistant_id);
            res.status(200).send({
                message:"successfully fetched patient requests.",
                status:200,
                data:assistant_request_details
            })
        

    }catch(e){
        next(e)
    }
}
module.exports={
    getPatientRequests
}