
let {getPatientDetailsService} = require("../../services/patientService/fetchPatient")
let {validateGetPatienDetails} = require("../../validator/validatePatientDetails")
let {Forbidden}= require("../../errorClasses/errorClass")
/**
 * @api {post} /api/v1/get-patient-details
 * @apiName Patient/Patient Request
 * @apiPermission ALL
 * @apiGroup Patient-Service 
    * @apiDescription  <h2>Get patient all details.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} patient_id  Mandatory : patient_id of the patient.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. successfully fetched patient details.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "successfully fetched doctor requests.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */
let getPatientDetails = async(req,res,next)=>{
    try{
        let opts = req.body;
        validateGetPatienDetails(opts)
        let patient_id = opts.patient_id;
        let patient_details = await getPatientDetailsService(patient_id);
        if(patient_details.length){
            let patient = patient_details.pop();
            res.status(200).send({
                message:"successfully fetched patient details.",
                status:200,
                data:patient
            })
        }
        else{
            next(new Forbidden(res.__("No such patient exists.")));

        }
    }catch(e){
        next(e)
    }
}

module.exports={
    getPatientDetails
}