var aws = require('aws-sdk')
var accessKeyId1 =   "AKIAJAMR4HGD77F365BQ";
var secretAccessKey1 =  "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";

aws.config.update({
    accessKeyId: accessKeyId1,
    secretAccessKey: secretAccessKey1,
    region: 'ap-south-1'
});
var s3 = new aws.S3()
const{validatePatientDownload}= require("../../validator/validatePatientDownload")
/**
 * @api {post} /api/v1/patient-report-downloader
 * @apiName Patient/Patient Request
 * @apiPermission ALL
 * @apiGroup Patient-Service 
    * @apiDescription  <h2>Download patient reports.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} fileUrl  Mandatory : S3 link of the patient reports.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Reporst successfully Downloaded.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Reports successfully Downloaded.",
*    "status": 200,
*    "file": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */



let patientDownloadController = async(req,res,next)=>{
    try{
        let opts =req.body;
       validatePatientDownload(opts);
        let fileUrl = opts.url;
        let key = (fileUrl.split('/')).pop()
        s3.getObject(
          { Bucket: "ectosence-patient-reports", Key: key },
          function (error, data) {
            if (error != null) {
            //   next("Failed to retrieve an object: " + error);
            next(new Forbidden(res.__("errors.forbidden")));

        } else {
              res.send({
                  message:"Reports successfully Downloaded.",
                  status:200,
                  file:data.Body
                
                });
              // do something with data.Body
            }
          }
        );

    }catch(e){
        next(e)
    }
}

module.exports={
    patientDownloadController
}