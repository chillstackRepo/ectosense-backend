const {upload} = require('../../services/uploadService/s3Uploader');
const {updatePatientReportRequest} = require('../../services/patientService/patientService')
const singleUpload = upload.single('image');
const {validatePatientUpload} = require("../../validator/validatePatientUpload")
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');

/**
 * @api {post} /api/v1/patient-report-uploader
 * @apiName Patient/Patient Request
 * @apiPermission ALL
 * @apiGroup Patient-Service 
    * @apiDescription  <h2>Upload reports by doctor.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} patient_request_id  Mandatory : patient_request_id of the patient.<br>
  * @apiParam {String} file  Mandatory : reports of patient.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Reports successfully uploaded.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Reports successfully uploaded.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let patientUploadController = async(req,res,next)=>{
    try{
        if(req.user.role=='patient' || 
        req.user.role=='admin' ||
        req.user.role=='assistant' ||
        req.user.role=='doctor'){
        singleUpload(req, res, async function(err) {
            let opts =req.body;
            validatePatientUpload(opts,req);
            let patient_request_id = opts.patient_request_id;
            let fileURL = (req.file.location).toString();
            let operator = req.user.registration_id;
            let role = req.user.role;    
            if (err) {
              return res.status(422).send({errors: [{title: 'File Upload Error', detail: err.message}] });
            }
            await updatePatientReportRequest(patient_request_id,fileURL,operator,role);
            res.status(200).send({
                message:`Reports successfully uploaded .`,
                status:200,
                data: {'imageUrl': req.file.location}
              })
        
          });
        } 
        else{
            next(new Forbidden(res.__("errors.forbidden")));
        }
    }catch(e){
        next(e)
    }
}

module.exports={patientUploadController}


