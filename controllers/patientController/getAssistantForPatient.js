const { readQuery, writeQuery } = require("../../queryExecution/prodQueryExecution");
let TransactionQueryService = require("../../queryExecution/transactionQuery");
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');


let getAssistant = async(patient_details,patient_request_id)=>{
    try{
    let patient = patient_details.pop(); 
    let disease_id = patient.disease_id;
    let expected_time_slot = patient.expected_time_slot;
    let clinic_id = patient.clinic_id;
    let selected_doctors_query = `select 
    distinct d.id as doctor_id,dar.assistant_id
    from 
    doctor_disease_map ddm,
    doctors d,
    doctor_schedules ds,
    doctor_assistant_relationship dar,
    clinic_doctor_relationship cdr,
    clinic_assistant_relationship car 
    where 
        ddm.doctor_id=d.id and 
        d.doctor_status='active' and
        ddm.disease_id = ?  and
        ddm.doctor_id = ds.doctor_id and
        ds.time_slot_status=0 and
        d.id = dar.doctor_id and 
        cdr.doctor_id = d.id and
		car.assistant_id = dar.assistant_id and 
        cdr.clinic_id = ? and
		car.clinic_id = ? and
        cdr.relation_status='active' and
		car.relation_status='active' and
        ds.time_slot_id between ? and ?;`

    let selected_assistants = await readQuery(selected_doctors_query,[disease_id,clinic_id,clinic_id,expected_time_slot,expected_time_slot+2])
    if(selected_assistants.length){
    let assistant_details = selected_assistants.pop();
    let {assistant_id} = assistant_details;
    let query = []
    query.push(`insert into assistant_requests (assistant_id,patient_request_id,request_status,created_dt,created_by,updated_dt,updated_by) VALUES (${assistant_id},${patient_request_id},'Pending',unix_timestamp(now()),1,unix_timestamp(now()),1)`)
    query.push(`update patient_requests set assistant_id=${assistant_id} and request_status='Processing' and updated_dt = unix_timestamp(now())`);
    await TransactionQueryService.multiQuery(query);
    return ({selected_assistants:assistant_id}) 
    }else{
        let request_query = `select created_dt from patient_requests where id =?;`
        let request = (await readQuery(request_query,[patient_request_id])).pop();
        let {created_dt} = request;
        let server_time = parseInt(Date.now() / 1000);
        if((server_time-created_dt)>300){
        let query_1 = `update patient_requests 
        set  updated_dt = unix_timestamp(now()),request_status= (case 
        when (unix_timestamp(now())-created_dt)>300 then 'Failed' else 'Pending' end)  where id = ?`
        await writeQuery(query_1,[patient_request_id])
        return ({selected_assistants:-1}) 
          }else{
        next(new Forbidden(res.__("No avaialiable assistants.Please try later.")));
          }
    }
    }catch(e){
        throw(e)
    }


}

module.exports={
    getAssistant
}