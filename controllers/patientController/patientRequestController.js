let {patientRequestService}=require("../../services/patientService/patientService");
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');
const {pushToPatientSNS}= require("../../services/snsService/patientSnsService")
const {validatePatientRequest}= require("../../validator/validatePatientRequest")

/**
 * @api {post} /api/v1/patient-treatment-request
 * @apiName Patient/Patient Request
 * @apiPermission ALL
 * @apiGroup Patient-Service 
    * @apiDescription  <h2>Create Patient Request.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
  * @apiParam {String} patient_id  Mandatory : patient_id of the patient.<br>
  * @apiParam {String} disease_id  Mandatory : disease_id of the patient.<br>
  * @apiParam {String} clinic_id  Mandatory : clinic_id of the patient.<br>
  * @apiParam {String} time_slot  Mandatory : expected time_slot of the patient.<br>
  * @apiParam {String} patient_reports  Mandatory : patient_reports of the patient.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Successfully created treatment requests.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Successfully created treatment requests.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let patientRequestController = async(req,res,next)=>{
    try{
        if(req.user.role=='patient' || 
        req.user.role=='admin' ||
        req.user.role=='assistant' ||
        req.user.role=='doctor'){
        let opts = req.body;
        validatePatientRequest(opts)
        let patient_id = opts.patient_id;
        let operator = req.user.registration_id;
        let role = req.user.role;
        
        let {status,request_id} = await patientRequestService(opts,patient_id,operator,role);
        if(status){
            let data ={
                message:"Successfully created treatment requests.",
                patient_id:patient_id,
                patient_request_id:request_id
            }  
            await pushToPatientSNS(data)
            res.status(200).send({
              status:200,
              data: data
            })

        }else{
             next(new InternalServerError(res.__("errors.genericMessage")));

        }

        }else{
             next(new Forbidden(res.__("errors.forbidden")));

        }

    }catch(e){
        next(new InternalServerError(res.__("errors.genericMessage")));
    }
}

module.exports={
    patientRequestController
}