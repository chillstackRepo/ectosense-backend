const { readQuery } = require('../../queryExecution/prodQueryExecution');
// const { getUserQuery } = require('../../mysqlQueries/authQuery/authQuery') 
const{ Unauthorized,Forbidden } = require("../../errorClasses/errorClass")
const { promisify } = require('util');
const jwt = require('jsonwebtoken');
const catchAsync = require('../../utills/catchAsync');
const config = require('../../config');
  

exports.protect = catchAsync(async (req, res, next) => {
    // 1) Getting token and check of it's there
    try{
    let token;
    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith('Bearer')
    ) {
      token = req.headers.authorization.split(' ')[1];
    } else if (req.cookies.jwt) {
      token = req.cookies.jwt;
    }
    if (!token) {
      return next(new Unauthorized(res.__('errors.unauthorised')));    
    }
  
    // 2) Verification token
    const decoded = await promisify(jwt.verify)(token, config.authControllerConfig.jwtDecrypter);
    //console.log(decoded.id)
    // 3) Check if user still exists
    let getUserQuery,input_id;
    if(decoded.role=='doctor'){
     getUserQuery = `select * from doctors where id = ?` 
     input_id = decoded.doctor_id;
     decoded['registration_id']=decoded.doctor_id;
    }else
     if(decoded.role=='patient'){
     getUserQuery = `select * from patients where id = ?` 
     input_id = decoded.patient_id;
     decoded['registration_id']=decoded.patient_id;

    }else
     if(decoded.role=='assistant'){
     getUserQuery = `select * from assistants where id = ?` 
     input_id = decoded.assistant_id;
     decoded['registration_id']=decoded.assistant_id;

    }else 
    if(decoded.role=='admin'){
      getUserQuery = `select * from admins where id = ?` 
      input_id = decoded.admin_id;
      decoded['registration_id']=decoded.admin_id;
     }else{
      return next(new Forbidden(res.__('errors.forbidden')));

    }
    console.log(getUserQuery,input_id);

     const currentUser = (await readQuery(getUserQuery,[input_id])).pop();

    if (!currentUser) {
     return next(new Forbidden(res.__('errors.userNotExists')));
    }
  
    // GRANT ACCESS TO PROTECTED ROUTE
    req.user = decoded;
    console.log(req.user.role)

    next();
  }
catch(e){
throw(e)
}
}
  );

  