const {checkIfDoctorAlreadyRegistered,checkIfAssistantAlreadyRegistered,checkIfAdminAlreadyRegistered,checkIfPatientAlreadyRegistered,doctorRegistration,patientRegistration,assistantRegistration,adminRegistration} = require('../../services/authServices/registrationServices')
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');
const {validateSignUp } = require("../../validator/validateSignUp")
/**
 * @api {post} /api/v1/registration
 * @apiName Registration 
 * @apiPermission ALL
 * @apiGroup Generic-Service 
    * @apiDescription  <h2>SignUp for patient/doctor/assistant/admin.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
  * @apiParam {String} role  Mandatory : User Role.<br>
  * @apiParam {String} phone  Mandatory : Registered Mobile number.<br>
  * @apiParam {String} doctor_password/patient_password/assistant_password/admin_password  Mandatory : Passcode <br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Successfully signed up.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Successfully signed up.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "Already registered, Please try to login."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let registrationtController = async(req,res,next)=>{
    try{
    let opts = req.body;
    validateSignUp(opts)
    let role = opts.role;
    let phone = opts.phone;
    if(role=='doctor'){
       let isAlreadyExist = await checkIfDoctorAlreadyRegistered(phone);
        if(isAlreadyExist){
            let error = new Forbidden('Already registered, Please try to login.');
            next(error)
        }
        else{
            let {status,doctor_id} = await doctorRegistration(opts);
            if(status){
                res.status(200).send({
                  status:200,
                  data:{
                      role:`doctor`,
                      registration_id:doctor_id
                  }  
                })
            }else{
                let error = new InternalServerError('Internal Server Error.');
                next(error)
    
            }

        }
    }else if(role=='assistant'){
        let isAlreadyExist = await checkIfAssistantAlreadyRegistered(phone);
        if(isAlreadyExist){
            let error = new Forbidden('Already registered, Please try to login.');
            next(error)
        }else{
            let {status,assistant_id} =  await assistantRegistration(opts);
            if(status){
                res.status(200).send({
                  status:200,
                  data:{
                      role:`assistant`,
                      registration_id:assistant_id
                  }  
                })
            }else{
                let error = new InternalServerError('Internal Server Error.');
                next(error)
    
            }

        }

    }else if(role=='patient'){
        let isAlreadyExist = await checkIfPatientAlreadyRegistered(phone);
        if(isAlreadyExist){
            let error = new Forbidden('Already registered, Please try to login.');
            next(error)
        }else{
            let {status,patient_id} =  await patientRegistration(opts);
            if(status){
                res.status(200).send({
                  status:200,
                  data:{
                      role:`patient`,
                      registration_id:patient_id
                  }  
                })
            }else{
                let error = new InternalServerError('Internal Server Error.');
                next(error)
    
            }


        }


    }else if(role=='admin'){
        let isAlreadyExist = await checkIfAdminAlreadyRegistered(phone);
        if(isAlreadyExist){
            let error = new Forbidden('Already registered, Please try to login.');
            next(error)
        }else{
            let {status,admin_id} =  await adminRegistration(opts);
            if(status){
                res.status(200).send({
                  status:200,
                  data:{
                      role:`admin`,
                      registration_id:admin_id
                  }  
                })
            }else{
                let error = new InternalServerError('Internal Server Error.');
                next(error)
    
            }

        }

    }


    }
    catch(e){
        next(e)
    }
    }

    module.exports={
        registrationtController
    }