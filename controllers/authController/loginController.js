const { readQuery,writeQuery } = require("../../queryExecution/prodQueryExecution");
const {secretKey} = require('../../config').registrationControllerConfig;
let jwt = require('jsonwebtoken');
const{validateLogin}=require("../../validator/validateLogin")
const { Forbidden, InternalServerError,Unauthorized } = require('../../errorClasses/errorClass');
/**
 * @api {post} /api/v1/login
 * @apiName Login 
 * @apiPermission ALL
 * @apiGroup Generic-Service 
    * @apiDescription  <h2>Login for patient/doctor/assistant/admin.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
  * @apiParam {String} role  Mandatory : User Role.<br>
  * @apiParam {String} phone  Mandatory : Registered Mobile number.<br>
  * @apiParam {String} password  Mandatory : Passcode <br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. Successfully logged in.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Successfully logged in.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let loginController = async(req,res,next)=>{
try{
    let opts = req.body;
    validateLogin(opts)
    let role = opts.role;
    let phone = opts.phone;
    let password = opts.password
    if (role == "doctor") {
      let query = `select id,doctor_password,doctor_phone,doctor_name from doctors where doctor_phone = ?`;
      let doctor_details = await readQuery(query, [phone]);
      if (doctor_details.length) {
          let { doctor_password,id,doctor_phone,doctor_name} = doctor_details.pop();
          if (
            doctor_phone == phone &&
            doctor_password == password
          ) {
            let token = jwt.sign({ role: 'doctor',doctor_id:id,doctor_phone:doctor_phone,doctor_password:doctor_password },secretKey,{ expiresIn: 60*60*24 });
            let update_doctor_token = `update doctors set jwt_token = ? where id = ?;`
            let update_response = await writeQuery(update_doctor_token,[token,id])
            if(update_response){
                res.status(200).send({
                    message:"Successfully logged in",
                    status: 200,
                    data: {
                      role: `doctor`,
                      registration_id: id,
                      doctor_name: doctor_name,
                      login: true,
                    },
                  });
      
            }else{
                return next(new InternalServerError(res.__("errors.genericMessage")));
            }
            
          } else {
            return next(new Forbidden(res.__("errors.forbidden")));
          }
        
      } else {
        return next(new Unauthorized(res.__("errors.unauthorised")));
      }
    } else if (role == "assistant") {
        let query = `select id,assistant_password,assistant_phone,assistant_name from assistants where assistant_phone = ?`;
        let assistant_details = await readQuery(query, [phone]);
        if (assistant_details.length) {
            let { assistant_password,id,assistant_phone,assistant_name} = assistant_details.pop();
            if (
                assistant_phone == phone &&
                assistant_password == password
            ) {
              let token = jwt.sign({ role: 'assistant',assistant_id:id,assistant_phone:assistant_phone,assistant_password:assistant_password },secretKey,{ expiresIn: 60*60*24 });
              let update_assistant_token = `update assistants set jwt_token = ? where id = ?;`
              let update_response = await writeQuery(update_assistant_token,[token,id])
              if(update_response){
                  res.status(200).send({
                      message:"Successfully logged in",
                      status: 200,
                      data: {
                        role: `assistant`,
                        registration_id: id,
                        assistant_name: assistant_name,
                        login: true,
                      },
                    });
                }
            }
        }
               else {
          return next(new Unauthorized(res.__("errors.unauthorised")));
        }
  

    

    } else if (role == "patient") {
        let query = `select id,patient_password,patient_phone,patient_name from patients where patient_phone = ?`;
        let patient_details = await readQuery(query, [phone]);
        if (patient_details.length) {
            let { patient_password,id,patient_phone,patient_name} = patient_details.pop();
            if (
                patient_phone == phone &&
                patient_password == password
            ) {
              let token = jwt.sign({ role: 'patient',patient_id:id,patient_phone:patient_phone,patient_password:patient_password },secretKey,{ expiresIn: 60*60*24 });
              let update_patient_token = `update patients set jwt_token = ? where id = ?;`
              let update_response = await writeQuery(update_patient_token,[token,id])
              if(update_response){
                  res.status(200).send({
                    message:"Successfully logged in",
                      status: 200,
                      data: {
                        role: `patient`,
                        registration_id: id,
                        patient_name: patient_name,
                        login: true,
                      },
                    });
        
              }else{
                  return next(new InternalServerError(res.__("errors.genericMessage")));
              }
              
            } else {
              return next(new Forbidden(res.__("errors.forbidden")));
            }
          
        }
    } else if (role == "admin") {
        let query = `select id,admin_password,admin_phone,admin_name from admins where admin_phone = ?`;
        let admin_details = await readQuery(query, [phone]);
        if (admin_details.length) {
            let { admin_password,id,admin_phone,admin_name} = admin_details.pop();
            if (
                admin_phone == phone &&
                admin_password == password
            ) {
              let token = jwt.sign({ role: 'admin',admin_id:id,admin_phone:admin_phone,admin_password:admin_password },secretKey,{ expiresIn: 60*60*24 });
              let update_admin_token = `update admins set jwt_token = ? where id = ?;`
              let update_response = await writeQuery(update_admin_token,[token,id])
              if(update_response){
                  res.status(200).send({
                    message:"Successfully logged in",
                      status: 200,
                      data: {
                        role: `admin`,
                        registration_id: id,
                        admin_name: admin_name,
                        login: true,
                      },
                    });
        
              }else{ 
  return next(new InternalServerError(res.__("errors.genericMessage")));
}

} else {
return next(new Forbidden(res.__("errors.forbidden")));
}

} else {
return next(new Unauthorized(res.__("errors.unauthorised")));
}




    } else {
          return next(new Unauthorized(res.__("errors.unauthorised")));
        }
  
            
}catch(e){
    next(e)
}
}

module.exports={
    loginController
}