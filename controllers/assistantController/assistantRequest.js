let {getAssistantRequestService}=require("../../services/assistantService/fetchAssistantRequest")
let {validateGetAssistantRequests} = require("../../validator/validateGetAssistantRequests")
/**
 * @api {post} /api/v1/get-assistant-requests
 * @apiName Asistant/Assistant Request
 * @apiPermission ALL
 * @apiGroup Assistant-Service 
    * @apiDescription  <h2>Get all assistant requests.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} assistant_id  Mandatory : assistant_id of the assistant.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. successfully fetched assistant requests..
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "successfully fetched assistant requests.",
*    "status": 200,
*   "data": [
*      {
*         "id": 1,
*        "assistant_id": 1,
*        "patient_request_id": 13,
*        "request_status": "Pending",
*        "created_dt": 1602886118,
*        "created_by": 1,
*        "updated_dt": 1602886118,
*        "updated_by": 1,
*       "updated_by_designation": null
*     },
*    {
*       "id": 2,
*       "assistant_id": 1,
*       "patient_request_id": 11,
*       "request_status": "Pending",
*       "created_dt": 1602886286,
*       "created_by": 1,
*       "updated_dt": 1602886286,
*       "updated_by": 1,
*       "updated_by_designation": null
*     }
*    ]
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */

let getAssistantRequests = async(req,res,next)=>{
    try{
        let opts = req.body;
         validateGetAssistantRequests(opts);
        let assistant_id = opts.assistant_id;
        let assistant_request_details = await getAssistantRequestService(assistant_id);
            res.status(200).send({
                message:"successfully fetched assistant requests.",
                status:200,
                data:assistant_request_details
            })
        

    }catch(e){
        next(e)
    }
}
module.exports={
    getAssistantRequests
}