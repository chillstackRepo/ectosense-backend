const {createDoctorRequestAndChangeStatus} = require("../../services/assistantService/createDoctorRequestService")
const { Forbidden, InternalServerError } = require('../../errorClasses/errorClass');
const {validateAssignDoctorToPatient} = require("../../validator/validateAssignDoctorToPatient")

/**
 * @api {post} /api/v1/assign-doctor-to-patient
 * @apiName Assistant/Assistant Request
 * @apiPermission Assistant/Admin
 * @apiGroup Assistant-Service 
    * @apiDescription  <h2>Appoint doctor for patient.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
  * @apiParam {String} doctor_id  Mandatory : doctor_id of the patient assistant.<br>
  * @apiParam {String} assistant_request_id  Mandatory : assistant_request_id of assistant.<br>
  * @apiParam {String} selected_time_slot  Mandatory : selected_time_slot by the assistant.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. successfully fetched doctor details.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Appointment successfully scheduled.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */
let assignDoctorToPatientController = async(req,res,next)=>{
    try{
        if(req.user.role=='assistant' || req.user.role=='admin'){
        let opts = req.body;
        validateAssignDoctorToPatient(opts)
        let doctor_id = opts.doctor_id;
        let assistant_request_id = opts.assistant_request_id;
        let selected_time_slot = opts.selected_time_slot;
        let operator = req.user.registration_id;
        let role = req.user.role;

        let {doctor_request_id} = await createDoctorRequestAndChangeStatus(doctor_id,assistant_request_id,selected_time_slot,operator,role);
        
        res.status(200).send({
            message:`Appointment successfully scheduled.`,
            status:200,
            data:{
            doctor_request_id:doctor_request_id,
            time_slots:selected_time_slot}
        })
    }else{
        next(new Forbidden(res.__("errors.forbidden")));

    }

    }catch(e){
        next(e)
    // next(new InternalServerError(res.__("errors.genericMessage")));
}
}

module.exports={
    assignDoctorToPatientController
}