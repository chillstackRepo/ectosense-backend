let {getAssistantDetailsService} = require("../../services/assistantService/fetchAssistant")
let {validateGetAssistantDetails} = require("../../validator/validateGetAssistantDetails")
let {Forbidden}= require("../../errorClasses/errorClass")

/**
 * @api {post} /api/v1/get-assistant-details
 * @apiName Assistant/Assistant Request
 * @apiPermission ALL
 * @apiGroup Assistant-Service 
    * @apiDescription  <h2>Get assistant all details.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} assistant_id  Mandatory : assistant_id of the assistant.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. successfully fetched assistant details.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "successfully fetched doctor requests.",
*    "status": 200,
*    "data": {}
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */
let getAssistantDetails = async(req,res,next)=>{
    try{
        let opts = req.body;
        validateGetAssistantDetails(opts)
        let assistant_id = opts.assistant_id;
        let assistant_details = await getAssistantDetailsService(assistant_id);
        if(assistant_details.length){
            let assistant = assistant_details.pop();
            res.status(200).send({
                message:"successfully fetched assistant details.",
                status:200,
                data:assistant
            })
        }
        else{
            next(new Forbidden(res.__("No such assistant exists.")));

        }
    }catch(e){
        next(e)
    }
}

module.exports={
    getAssistantDetails
}