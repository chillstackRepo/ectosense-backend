let {selectDoctorListService} = require("../../services/assistantService/assistantService")
let {validateDoctorListForPatient} = require("../../validator/validatedoctorListForPatientController")
let {Forbidden}= require("../../errorClasses/errorClass")

/**
 * @api {post} /api/v1/get-doctor-list-for-assistant
 * @apiName Assistant/Assistant Request
 * @apiPermission ALL
 * @apiGroup Assistant-Service 
    * @apiDescription  <h2>Get doctor list for assistant.</h2>
    * Repository: "ectosense"<br>
    * Ticket Id : None <br>
    * Ticket Link : None <br>
    * <hr>
    
  * @apiParam {String} patient_request_id  Mandatory : patient_request_id of the patient.<br>
  * @apiParam {String} assistant_id  Mandatory : assistant_id of the patient assistant.<br>
  * @apiSuccess (Success) {json} response-body  body attached in success example.
  * @apiSuccess (Success) {Number} status 200
  * @apiError (Forbidden) {String} message Ex. successfully fetched doctor details.
  * @apiError (Forbidden) {Number} status 403
  * @apiError (InternalServerError) {String} message Something went wrong.
  * @apiError (InternalServerError) {Number} status 500
  * @apiError (Unauthorized) {String} message You are not logged in! Please log in to get access.
  * @apiError (Unauthorized) {Number} status 401
  
  * @apiSuccessExample Success-Response:
*  {
*     "message": "Successfully fetch doctor details for patient.",
*    "status": 200,
*    "data": []
*  }
  * @apiErrorExample Error-Response:
    *     HTTP/1.1 403 Forbidden
    *     {
    *      "You do not have permission to perform this action."
    *     }
    *     HTTP/1.1 500 InternalServerError
    *     {
    *      "Something went wrong."
    *     }
    *     HTTP/1.1 401 Unauthorized
    *     {
    *      "You are not logged in! Please log in to get access."
    *     }

    */
let doctorListForPatientController = async(req,res,next)=>{
    try{

        if(req.user.role=='assistant' || req.user.role=='admin'){
        let opts = req.body;
        validateDoctorListForPatient(opts)
        let patient_request_id = opts.patient_request_id;
        let assistant_id = opts.assistant_id;
        let doctorList = await selectDoctorListService(assistant_id,patient_request_id);
        res.status(200).send({
            message:'Successfully fetch doctor details for patient.',
            status:200,
            data:doctorList
        })
    }else{
        next(new Forbidden(res.__("errors.forbidden")));

    }
    }catch(e){
        next(e)
    }
}

module.exports={
    doctorListForPatientController
}