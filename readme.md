# Ectosense Clinics

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)]()

The aim of provide medical facility to user with help of tele medicine or real time treatment.Via this platform we can create and oerate clinics. 
 

#  Features 
  -  create clinic
  - operate clinic

You can also:
  - We can onboard assistant and doctor to run clinics.
  - We can create clinic clusters as well.
  - Patient can be facilate by clinic medical service.

### Installation

Dillinger requires [Node.js](https://nodejs.org/) v8+ to run.

Install the dependencies and start the server.
```sh
$ npm install 
$ npm start
```

## Running Status

Server configured to autostart with port 1000.Here is the example of running server in terminal.
```sh
ERVER IS LISTENING ON :7000
```



### Todos
 - Set up  Redis

License
----

Ectosense




 [paymentApidoc]: <https://github.com/joemccann/dillinger>
