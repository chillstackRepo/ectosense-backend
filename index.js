/**
 * @requires express Using For Express JS
 * @requires http For creating http server
 * @requires payment-microService To import all the routes
 * @requires body-parser For parsing incoming data is coming
 * @requires logger To put logs via pino
 *
 */
const express = require("express");
const app = express();
const http = require("http");
const bodyParser = require("body-parser");
const globalErrorHandler = require("./controllers/errorController/errorController");
const routes = require("./routes/routes");
const helmet = require("helmet");
const i18n = require("i18n");
const rateLimit = require("express-rate-limit");
const xss = require("xss-clean");
const hpp = require("hpp");
const cookieParser = require("cookie-parser");
const compression = require("compression");
const config = require('./config')
app.enable("trust proxy");
app.use(helmet());

i18n.configure({
  locales: ["en", "hi", "te"],
  defaultLocale: "en",
  directory: `${__dirname}/locales`,
  objectNotation: true,
  updateFiles: false,
  queryParameter: "lang",
});


const limiter = rateLimit({
  max: 10000,
  windowMs: 1 * 60 * 1000,
  message: "Too many requests from this IP, please try again in sometime!",
});

app.use(i18n.init);
app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.urlencoded({ extended: true, limit: "10kb" }));
app.use(cookieParser());
app.use(xss());
app.use(hpp());
app.use(compression());
app.use("/api/v1/", limiter);
app.use("/api/v1/",routes);

app.all("*", (req, res, next) => {
  next(new Error(res.__('errors.badRequest')));
});

app.use(globalErrorHandler);

/** Server Running on 8082
 * @constant server To create server
 * @constant port To set the port in which server is running
 */

const server = http.createServer(app);
const port = config.yuluServerConfig.paymentPort;
server.listen(port, function () {
  console.info("SERVER IS LISTENING ON :" + port);
});
